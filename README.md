# ESIH android app
L’application Android « ESIH » est une plateforme cliente donnant la possibilité à un utilisateur de prendre part à des informations qui lui aurait été inaccessibles au moment de leurs publications, des informations pertinentes à sa formation et des ressources utiles :
Il sera informer des activités de l’école (avis de la Direction, horaires…), des activités d’organismes proches de l’école ou en rapport avec le/les domaine(s) enseigner (concours, offres d’emploi….).
il aura accès à des supports de cours (devoirs, documents, vidéos…).
Il bénéficiera des connaissances d’autres étudiants via un forum de discussion.
Il aura accès à des informations personnelles (Nombre de reprises, Balance dû, notes…) .
il sera en mesure de rendre ses devoirs de maison via cette plateforme.


**Fonctionnalités**

* [x] Une page d’acceuil contenant des publications significatives pour l’ étudiant ;ces informations pourront être accompagnées d’une images ou d’une vidéo
* [x] Une section permettant a l’administration de publier des devoirs, des vidéos de cours, des avis dédiés etc…
* [x] Une bibliothèque virtuelle avec lecteur intégré. 
* [x] Une section contenant les différents horaires de cours et d’examens.
* [x] Une fenêtre dédiée a la publication des résultats de l’utilisateur.
* [x] Un forum de discussion pour les étudiants de l’ESIH.
* [x] Un profil utilisateur contenant les principales informations de l’utilisateur.


**Autres fonctionnalités :**
* [x] Un outil de récupération de mot de passe.
* [x] Un outil de partage de document.
* [x] Facebook single sign-on.

**Libraries Open-source utilisées**

- [Android Async HTTP](https://github.com/loopj/android-async-http) - Simple asynchronous HTTP requests with JSON parsing
- [Picasso](http://square.github.io/picasso/) - Image loading and caching library for Android

**Captures d'écran**
<br/>
<img src='https://github.com/emrood/ESIH/blob/master/Docs/horaire.gif' title='Video Walkthrough' width='' alt='Video Walkthrough' /><br/>
<img src='https://github.com/emrood/ESIH/blob/master/Docs/bibliotheque.gif' title='Video Walkthrough' width='' alt='Video Walkthrough' /><br/>
<img src='https://github.com/emrood/ESIH/blob/master/Docs/profil.gif' title='Video Walkthrough' width='' alt='Video Walkthrough' /><br/>

**License**

    Copyright 2017 - Noel Emmnanuel Roodly

    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

        http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.
    
    

