package fr.emmanuelroodlyyahoo.esih.Activities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.text.TextUtils;
import android.text.format.DateUtils;
import android.util.Log;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.MediaController;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.VideoView;

import com.backendless.Backendless;
import com.backendless.BackendlessUser;
import com.backendless.async.callback.AsyncCallback;
import com.backendless.exceptions.BackendlessFault;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Locale;
import java.util.regex.Pattern;

import fr.emmanuelroodlyyahoo.esih.Models.Infos;
import fr.emmanuelroodlyyahoo.esih.R;
import fr.emmanuelroodlyyahoo.esih.utils.PatternEditableBuilder;
import jp.wasabeef.glide.transformations.RoundedCornersTransformation;

public class DetailActivity extends AppCompatActivity {

    TextView tvDetailTitle;
    TextView tvDetail;
    ImageView ivDetail;
    TextView tvViews;
    TextView tvDate;
    ImageView ivStar;
    ImageView ivShare;
    ImageView detailImage;
    VideoView detailVideo;
    Infos anInfo;
    BackendlessUser user;
    MainActivity mainActivity;
    ProgressBar pbDetailImage;
    Toolbar toolbarDetail;
    ImageButton ibBackDetail;
    ImageButton ibPlay;
    ProgressDialog pDialog;
    int vues;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);
        //Setting views
        initViews();
        setSupportActionBar(toolbarDetail);
        getSupportActionBar().setDisplayShowTitleEnabled(false);


        anInfo = (Infos) getIntent().getSerializableExtra("info");
        tvDetailTitle.setText(anInfo.getTitre());
        tvDetail.setText(Html.fromHtml(anInfo.getDetail()));
        pbDetailImage = (ProgressBar) findViewById(R.id.pbDetailImage);
        pbDetailImage.setVisibility(View.VISIBLE);
        DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
        //String date = df.format(anInfo.getCreated());

        if(anInfo.isImportant()){
            Glide.with(getBaseContext()).load(R.drawable.staryellow).into(ivStar);
        }else{
            Glide.with(getBaseContext()).load(R.drawable.stargray).into(ivStar);
        }

        ibBackDetail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        tvViews.setText("Vues: " + String.valueOf(anInfo.getViews()));
        tvDate.setText(getRelativeTimeAgo(anInfo.getCreated()));

        ivShare.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent sharingIntent = new Intent(Intent.ACTION_SEND);
                sharingIntent.setType("text/plain");
                sharingIntent.putExtra(Intent.EXTRA_TEXT, anInfo.getDetail());
                startActivity(Intent.createChooser(sharingIntent, "Share via"));
            }
        });

        Glide.with(getBaseContext())
                .load(anInfo.getPath())
                .listener(new RequestListener<String, GlideDrawable>() {
                    @Override
                    public boolean onException(Exception e, String model, Target<GlideDrawable> target, boolean isFirstResource) {
                        pbDetailImage.setVisibility(View.GONE);
                        return false; // important to return false so the error placeholder can be placed
                    }

                    @Override
                    public boolean onResourceReady(GlideDrawable resource, String model, Target<GlideDrawable> target, boolean isFromMemoryCache, boolean isFirstResource) {
                        pbDetailImage.setVisibility(View.GONE);
                        return false;
                    }
                })
                .bitmapTransform(new RoundedCornersTransformation(getBaseContext(), 30, 10))
                .placeholder(R.drawable.logo2).error(R.drawable.logo2).into(ivDetail);


        new PatternEditableBuilder().
                addPattern(Pattern.compile("\\#(\\w+)"), Color.CYAN,
                        new PatternEditableBuilder.SpannableClickedListener() {
                            @Override
                            public void onSpanClicked(String text) {
                                Toast.makeText(DetailActivity.this, "Clicked hashtag: " + text,
                                        Toast.LENGTH_SHORT).show();
                            }
                        }).into(tvDetail);

        if(!TextUtils.isEmpty(anInfo.getImg())){
            detailImage.setVisibility(View.VISIBLE);
            Glide.with(getBaseContext()).load(anInfo.getImg()).placeholder(R.drawable.bon).into(detailImage);

        }else if(!TextUtils.isEmpty(anInfo.getVideo())){
            detailVideo.setVisibility(View.VISIBLE);
            ibPlay.setVisibility(View.VISIBLE);
            ibPlay.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    ibPlay.setVisibility(View.GONE);
                    loadVideo();
                }
            });
        }

        //Update Views
        updateViews();

    }


    public void initViews(){
        toolbarDetail = (Toolbar) findViewById(R.id.toolbarDetail);
        tvViews = (TextView) findViewById(R.id.tvViews);
        ivStar = (ImageView) findViewById(R.id.ivStar);
        ivShare = (ImageView) findViewById(R.id.ivShare);
        tvDate = (TextView) findViewById(R.id.tvDate);
        detailImage = (ImageView) findViewById(R.id.detailImage);
        detailVideo = (VideoView) findViewById(R.id.detailVideo);
        ibBackDetail = (ImageButton) findViewById(R.id.ibBackDetail);
        tvDetail = (TextView) findViewById(R.id.tvDetail);
        tvDetailTitle = (TextView) findViewById(R.id.tvdetailTitle);
        ivDetail = (ImageView) findViewById(R.id.ivDetailInfo);
        ibPlay = (ImageButton) findViewById(R.id.ibPlay);
        detailImage.setVisibility(View.GONE);
        detailVideo.setVisibility(View.GONE);
        ibPlay.setVisibility(View.GONE);

    }

    // getRelativeTimeAgo("");
    public String getRelativeTimeAgo(String rawJsonDate) {
        String backendFormat = "MM/dd/yyyy HH:mm:ss";
        SimpleDateFormat sf = new SimpleDateFormat(backendFormat, Locale.ENGLISH);
        sf.setLenient(true);

        String relativeDate = "";
        try {
            long dateMillis = sf.parse(rawJsonDate).getTime();
            relativeDate = DateUtils.getRelativeTimeSpanString(dateMillis,
                    System.currentTimeMillis(), DateUtils.SECOND_IN_MILLIS).toString();
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return relativeDate;
    }


    public void updateViews(){
        vues = Integer.parseInt(String.valueOf(anInfo.getViews()));
        vues = vues + 1;
        anInfo.setObjectId(anInfo.getObjectId());
        anInfo.setViews(vues);
        AsyncTask.execute(new Runnable() {
            @Override
            public void run() {
               Backendless.Persistence.of(Infos.class).save(anInfo, new AsyncCallback<Infos>() {
                   @Override
                   public void handleResponse(Infos response) {

                   }

                   @Override
                   public void handleFault(BackendlessFault fault) {

                   }
               });
            }
        });
    }


    public void loadVideo(){
        // Create a progressbar
        pDialog = new ProgressDialog(DetailActivity.this);
        // Set progressbar title
        pDialog.setTitle("ESIH player");
        // Set progressbar message
        pDialog.setMessage("Chargement...");
        pDialog.setIndeterminate(false);
        pDialog.setCancelable(false);
        // Show progressbar
        pDialog.show();

        try {
            // Start the MediaController
            MediaController mediacontroller = new MediaController(
                    DetailActivity.this);
            mediacontroller.setAnchorView(detailVideo);
            // Get the URL from String VideoURL
            Uri video = Uri.parse(anInfo.getVideo());
            detailVideo.setMediaController(mediacontroller);
            detailVideo.setVideoURI(video);

        } catch (Exception e) {
            Log.e("Error", e.getMessage());
            e.printStackTrace();
        }

        detailVideo.requestFocus();
        detailVideo.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
            // Close the progress bar and play the video
            public void onPrepared(MediaPlayer mp) {
                pDialog.dismiss();
                detailVideo.start();
            }
        });

    }


    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
    }

    public void playMe(View view) {

    }
}
