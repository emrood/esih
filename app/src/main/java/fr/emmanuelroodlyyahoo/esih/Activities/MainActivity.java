package fr.emmanuelroodlyyahoo.esih.Activities;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.TargetApi;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.provider.MediaStore;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.ViewAnimationUtils;
import android.widget.FrameLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.backendless.Backendless;
import com.backendless.BackendlessUser;
import com.backendless.DeviceRegistration;
import com.backendless.Subscription;
import com.backendless.async.callback.AsyncCallback;
import com.backendless.exceptions.BackendlessFault;
import com.backendless.files.BackendlessFile;
import com.backendless.messaging.BodyParts;
import com.backendless.messaging.DeliveryOptions;
import com.backendless.messaging.MessageStatus;
import com.backendless.messaging.PublishOptions;
import com.backendless.messaging.PublishPolicyEnum;
import com.backendless.messaging.PushBroadcastMask;
import com.backendless.messaging.SubscriptionOptions;
import com.backendless.persistence.DataQueryBuilder;
import com.facebook.login.LoginManager;

import org.apache.commons.io.FilenameUtils;
import org.apache.commons.io.output.ByteArrayOutputStream;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import fr.emmanuelroodlyyahoo.esih.Fragments.Administration;
import fr.emmanuelroodlyyahoo.esih.Fragments.Credit;
import fr.emmanuelroodlyyahoo.esih.Fragments.Forum;
import fr.emmanuelroodlyyahoo.esih.Fragments.Horaire;
import fr.emmanuelroodlyyahoo.esih.Fragments.Info;
import fr.emmanuelroodlyyahoo.esih.Fragments.Notes;
import fr.emmanuelroodlyyahoo.esih.Fragments.Palmares;
import fr.emmanuelroodlyyahoo.esih.Fragments.Profil;
import fr.emmanuelroodlyyahoo.esih.Fragments.Ressources;
import fr.emmanuelroodlyyahoo.esih.Fragments.SendFile;
import fr.emmanuelroodlyyahoo.esih.Fragments.Settings;
import fr.emmanuelroodlyyahoo.esih.Models.AdminMessage;
import fr.emmanuelroodlyyahoo.esih.Models.Message;
import fr.emmanuelroodlyyahoo.esih.Models.Note;
import fr.emmanuelroodlyyahoo.esih.R;
import fr.emmanuelroodlyyahoo.esih.utils.DefaultCallback;
import fr.emmanuelroodlyyahoo.esih.utils.Defaults;

import static android.Manifest.permission.CAMERA;
import static android.Manifest.permission.READ_EXTERNAL_STORAGE;
import static android.Manifest.permission.WRITE_EXTERNAL_STORAGE;
import static com.google.android.gms.internal.zzagz.runOnUiThread;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener{

    public FrameLayout container;
    public DrawerLayout drawer;
    public NavigationView navigationView;
    TextView tvReturn;
    TextView tvEmail;
    BackendlessUser user;
    public FloatingActionButton fab;
    public Intent d;
    SharedPreferences mUser;//Pour la sauvegarde des donnees d'authentification
    SharedPreferences.Editor editor;
    DeliveryOptions deliveryOptions;
    public String deviceId = null;//Pour la sauvegarde de l'ID de l'appareil sur Backendless
    public static String optSelected;

    private static final int REQUEST_READ_STORAGE = 0;
    private static final int REQUEST_CAMERA = 0;
    private static final int REQUEST_WRITE_STORAGE = 0;
    static final int REQUEST_IMAGE_CAPTURE = 50;
    private static final int CHOOSE_FILE_REQUESTCODE = 30;
    static final int IMAGE_FROM_GALERY = 82;
    public Bitmap mImageBitmap;
    private String mCurrentPhotoPath;
    Profil profil;
    FragmentManager fm;
    SendFile sendFile;
    public ArrayList<Note> session1;
    public ArrayList<Note> session2;
    public ArrayList<AdminMessage> admin;
    public ArrayList<Message> forum;
    DataQueryBuilder session1_query;
    DataQueryBuilder session2_query;
    DataQueryBuilder admin_query;
    DataQueryBuilder forum_query;
    public static int PAGESIZE = 20;
    public boolean result = false;
    MainActivity mainActivity;
    public int r = 0;
    public File avatarFile = null;
    public String avatarUrl = " ";
    Credit credit;
    public Bitmap bitmap = null;
    public byte[] avatar_byteArray;
    public  List<String> emailList;
    public BodyParts bodyParts;
    public String nom_fichier;
    private ProgressDialog progressDialog;
    public PublishOptions publishOptions;
    public SubscriptionOptions subscriptionOptions;
    public Subscription subscription;
    Uri imageUri;
    Boolean exitApp = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Backendless.setUrl( Defaults.SERVER_URL );
        Backendless.initApp( getApplicationContext(), Defaults.APPLICATION_ID, Defaults.API_KEY );
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        toolbar.setTitle("Acceuil");
        deliveryOptions = new DeliveryOptions();
        mUser = getBaseContext().getSharedPreferences("User", Context.MODE_PRIVATE);
        user = Backendless.UserService.CurrentUser();
        container = (FrameLayout) findViewById(R.id.flContent);
        fab = (FloatingActionButton) findViewById(R.id.fab);
        profil = (Profil) getSupportFragmentManager().findFragmentById(R.id.Profil);
        emailList = new ArrayList<String>();

        emailList.add("esih.devteam@gmail.com");
        emailList.add(user.getEmail());
        bodyParts = new BodyParts();

        publishOptions = new PublishOptions();
        publishOptions.setPublisherId(user.getProperty("name").toString());
        publishOptions.setSubtopic("forum");

        subscriptionOptions = new SubscriptionOptions();
        subscriptionOptions.setSubtopic("forum");

        fm = getSupportFragmentManager();
        sendFile = new SendFile();
        credit = new Credit();
        session1 = new ArrayList<>();
        session2 = new ArrayList<>();
        admin = new ArrayList<>();
        forum = new ArrayList<>();
        mainActivity = new MainActivity();
        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();


        navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        View hView = navigationView.getHeaderView(0);

        tvEmail = (TextView) hView.findViewById(R.id.tvEmail);
        if(user != null){
            tvEmail.setText(user.getEmail());
        }else{
            tvEmail.setText("@user");
        }

        //Permet de recevoir l'ID de l'appareil pour les notifications push
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                getDeviceRegistration();
                if(deviceId !=  null){
                    //publishOptions = new PublishOptions(deviceId);
                }
            }
        }, 2000);

        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                requestPermission();
                sendFile.show(fm, "SEND");
            }
        });


        View tool = toolbar.getFocusedChild();
        tvReturn = (TextView) findViewById(R.id.toolbar).findViewById(R.id.tvReturn);
        //tvReturn = (TextView) tool.findViewById(R.id.tvReturn);
        tvReturn.setVisibility(View.GONE);



        if(!requestCamera()){
            //Should request an acces
        }
        if(!requestExternalStoragePermission()){
            //Should request an acces
        }
        if(!requestWriteStoragePermission()){
            //Should request an acces
        }
        if(!requestWriteStoragePermission() || !requestExternalStoragePermission()){

        }

        if(user == null){
            Intent i = new Intent(MainActivity.this, LoginActivity.class);
            startActivity(i);
            Toast.makeText(MainActivity.this, "Veuillez vous reconnecter", Toast.LENGTH_SHORT).show();
        }

        if(savedInstanceState == null){
            Info homeFragment = new Info();
            FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
            ft.replace(R.id.flContent, homeFragment);
            ft.commit();
        }


        AsyncTask.execute(new Runnable() {
            @Override
            public void run() {
                populateAdminMessages();
                populateMessages();
                if(r == 4){
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            //Toast.makeText(MainActivity.this, "data loaded", Toast.LENGTH_SHORT).show();
                        }
                    });

                }else{
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                           //Toast.makeText(MainActivity.this, "error loading data", Toast.LENGTH_SHORT).show();
                        }
                    });

                }
            }
        });

    }

    public void requestPermission(){
        if(!requestWriteStoragePermission() && !requestExternalStoragePermission()){
            return;
        }

    }


    public void subscription(){

        Backendless.Messaging.subscribe(Defaults.DEFAULT_CHANNEL, new AsyncCallback<List<com.backendless.messaging.Message>>() {
            @Override
            public void handleResponse(List<com.backendless.messaging.Message> response) {
                Log.d("DEGUB", response.toString());
            }

            @Override
            public void handleFault(BackendlessFault fault) {
                Log.d("debug", fault.getMessage());
            }
        });
    }


    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            if(exitApp){
                //System.gc();
                //System.exit(0);
                moveTaskToBack(true);
            }else{
                Toast.makeText(this, R.string.quit_message, Toast.LENGTH_SHORT).show();
                exitApp = true;
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        exitApp = false;
                    }
                }, 3 * 1000);
            }

        }

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            signOut();
            return true;
        }
        if (id == R.id.action_credit) {
            credit.show(fm, "SEND");
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.

        Fragment fragment = null;
        Class fragmentClass;
        int id = item.getItemId();
        switch(item.getItemId()) {
            case R.id.infos:
                fragmentClass = Info.class;
                break;
            case R.id.horaire:
                fragmentClass = Horaire.class;
                break;
            case R.id.Profil:
                fragmentClass = Profil.class;
                break;
            case R.id.Forum:
                fragmentClass = Forum.class;
                break;
            case R.id.settings:
                fragmentClass = Settings.class;
                break;
            case R.id.Ressources:
                fragmentClass = Ressources.class;
                break;
            case R.id.message:
                fragmentClass = Administration.class;
                break;
            case R.id.palmares:
                fragmentClass = Palmares.class;
                break;
            default:
                fragmentClass = Info.class;
        }

        try {
            fragment = (Fragment) fragmentClass.newInstance();
        } catch (Exception e) {
            e.printStackTrace();
        }

        // Insert the fragment by replacing any existing fragment
        FragmentManager fragmentManager = getSupportFragmentManager();
        //fragmentManager.beginTransaction().replace(R.id.flContent, fragment).commit();
        FragmentTransaction fts = getSupportFragmentManager().beginTransaction();
        fts.setCustomAnimations(R.anim.right_in, R.anim.left_out);
        //fragmentManager.beginTransaction().replace(R.id.flContent, fragment).commit();
        fts.replace(R.id.flContent, fragment).commit();

        item.setChecked(true);

        setTitle(item.getTitle());

        drawer.closeDrawers();
        return true;
    }

    public Boolean isNetworkAvailable() {

                ConnectivityManager connectivityManager
                        = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
                NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
                return activeNetworkInfo != null && activeNetworkInfo.isConnectedOrConnecting();

    }

    public boolean isOnline() {
        Runtime runtime = Runtime.getRuntime();
        try {
            Process ipProcess = runtime.exec("/system/bin/ping -c 1 8.8.8.8");
            int     exitValue = ipProcess.waitFor();
            return (exitValue == 0);
        } catch (IOException e)          { e.printStackTrace(); }
        catch (InterruptedException e) { e.printStackTrace(); }
        return false;
    }

    public void signOut(){
        d = new Intent(MainActivity.this, LoginActivity.class);
        editor = mUser.edit();
        editor.putString("Email", "");
        editor.putString("Pass", "");
        editor.apply();
        LoginManager.getInstance().logOut();
        Backendless.UserService.logout(new AsyncCallback<Void>() {
            @Override
            public void handleResponse(Void response) {
                startActivity(d);

            }

            @Override
            public void handleFault(BackendlessFault fault) {
                Toast.makeText(MainActivity.this, fault.getMessage() , Toast.LENGTH_SHORT).show();
                startActivity(d);
            }
        });

    }

    public void enterReveal() {

        if (Build.VERSION.SDK_INT >= 21) {

            int cx = fab.getMeasuredWidth() / 2;
            int cy = fab.getMeasuredHeight() / 2;
            int finalRadius = Math.max(fab.getWidth(), fab.getHeight()) / 2;
            Animator anim = ViewAnimationUtils.createCircularReveal(fab, cx, cy, 0, finalRadius);
            fab.setVisibility(View.VISIBLE);
            anim.start();
        }


    }

    public void exitReveal() {
        // previously visible view
        if (Build.VERSION.SDK_INT >= 21) {
            int cx = fab.getMeasuredWidth() / 2;
            int cy = fab.getMeasuredHeight() / 2;

            int initialRadius = fab.getWidth() / 2;

            Animator anim = ViewAnimationUtils.createCircularReveal(fab, cx, cy, initialRadius, 0);

            anim.addListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    super.onAnimationEnd(animation);
                    fab.setVisibility(View.INVISIBLE);
                }
            });
            anim.start();
        }
    }

    public void getDeviceRegistration(){
        Backendless.Messaging.getDeviceRegistration(new AsyncCallback<DeviceRegistration>() {
            @Override
            public void handleResponse(DeviceRegistration response) {
                Log.d("DEBUG", response.toString());
                deviceId = response.getDeviceId();
            }

            @Override
            public void handleFault(BackendlessFault fault) {
                Log.d("DEBUG", fault.toString());
            }
        });
    }



    public  void pushNotification(){
        publishOptions = new PublishOptions();
        HashMap<String, String> headers = new HashMap<String, String>();
        headers.put("alert-text", "push for android");
        headers.put("android-ticker-text", "You just got a push notification!");
        headers.put("android-content-title", "This is a notification title");
        headers.put("android-content-text", "Push Notifications are cool");
        publishOptions.setHeaders(headers);
        deliveryOptions = new DeliveryOptions();
        deliveryOptions.setPublishPolicy(PublishPolicyEnum.BOTH);
        deliveryOptions.setPushBroadcast( PushBroadcastMask.ANDROID );
        Backendless.Messaging.publish("Hi Devices!", publishOptions, deliveryOptions, new AsyncCallback<MessageStatus>() {
            @Override
            public void handleResponse(MessageStatus response) {
                Log.d("DEBUG", response.toString());
            }

            @Override
            public void handleFault(BackendlessFault fault) {
                Log.d("DEBUG", fault.toString());
            }
        });

    }




    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (requestCode == REQUEST_IMAGE_CAPTURE && resultCode == RESULT_OK) {

            try {
                if(data != null){
                    imageUri = data.getData();
                    avatarUrl = data.getDataString();
                    avatarFile = new File(avatarUrl);
                    bitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), imageUri);
                    updateAvatar(bitmap);
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }else if (requestCode == CHOOSE_FILE_REQUESTCODE && resultCode == RESULT_OK){
            try {
                sendFile.Fpath = null;
                if(data != null){
                    sendFile.Fpath = data.getDataString();
                    imageUri = data.getData();
                }
                if(sendFile.Fpath != null) {
                    sendFile.fileChoosed = new File(sendFile.Fpath);
                    sendFile.fileName = sendFile.fileChoosed.getName();
                    sendFile.extension = FilenameUtils.getExtension(sendFile.Fpath);
                    sendFile.tvPicker.setText(sendFile.fileName);
                    sendFile.checkFilePicked();
                    sendFile.photo = MediaStore.Images.Media.getBitmap(this.getContentResolver(), imageUri);
                }

            } catch (IOException e) {
                e.printStackTrace();
            }

            super.onActivityResult(requestCode, resultCode, data);
        }
        /*
        if(requestCode == CHOOSE_FILE_REQUESTCODE && resultCode == RESULT_OK ){
            sendFile.Fpath = data.getDataString();
            sendFile.tvPicker.setText(sendFile.Fpath);
            sendFile.fileChoosed = new File(sendFile.Fpath);
            sendFile.checkFilePicked();
            super.onActivityResult(requestCode, resultCode, data);
        }*/
    }



    public boolean requestWriteStoragePermission() {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
            return true;
        }
        if (checkSelfPermission(WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
            return true;
        }
        if (shouldShowRequestPermissionRationale(WRITE_EXTERNAL_STORAGE)) {
            Snackbar.make(navigationView, R.string.permission_rationale, Snackbar.LENGTH_INDEFINITE)
                    .setAction(android.R.string.ok, new View.OnClickListener() {
                        @Override
                        @TargetApi(Build.VERSION_CODES.M)
                        public void onClick(View v) {
                            requestPermissions(new String[]{WRITE_EXTERNAL_STORAGE}, REQUEST_WRITE_STORAGE);
                        }
                    });
        } else {
            requestPermissions(new String[]{WRITE_EXTERNAL_STORAGE}, REQUEST_WRITE_STORAGE);
        }
        return false;
    }

    public boolean requestCamera() {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
            return true;
        }
        if (checkSelfPermission(CAMERA) == PackageManager.PERMISSION_GRANTED) {
            return true;
        }
        if (shouldShowRequestPermissionRationale(CAMERA)) {
            Snackbar.make(navigationView, R.string.permission_camera, Snackbar.LENGTH_INDEFINITE)
                    .setAction(android.R.string.ok, new View.OnClickListener() {
                        @Override
                        @TargetApi(Build.VERSION_CODES.M)
                        public void onClick(View v) {
                            requestPermissions(new String[]{CAMERA}, REQUEST_CAMERA);
                        }
                    });
        } else {
            requestPermissions(new String[]{CAMERA}, REQUEST_CAMERA);
        }
        return false;
    }

    public boolean requestExternalStoragePermission() {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
            return true;
        }
        if (checkSelfPermission(READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
            return true;
        }
        if (shouldShowRequestPermissionRationale(READ_EXTERNAL_STORAGE)) {
            Snackbar.make(navigationView, R.string.permission_rationale, Snackbar.LENGTH_INDEFINITE)
                    .setAction(android.R.string.ok, new View.OnClickListener() {
                        @Override
                        @TargetApi(Build.VERSION_CODES.M)
                        public void onClick(View v) {
                            requestPermissions(new String[]{READ_EXTERNAL_STORAGE}, REQUEST_READ_STORAGE);
                        }
                    });
        } else {
            requestPermissions(new String[]{READ_EXTERNAL_STORAGE}, REQUEST_READ_STORAGE);
        }
        return false;
    }










    public void populateAdminMessages() {
        admin_query = DataQueryBuilder.create();
        admin_query.setPageSize(PAGESIZE);
        admin_query.setSortBy("created DESC");
        String query = "niveau = " + "'" + user.getProperty("niveau") + "'";
        admin_query.setWhereClause(query);

        Backendless.Persistence.of(AdminMessage.class).find(admin_query, new AsyncCallback<List<AdminMessage>>() {
            @Override
            public void handleResponse(List<AdminMessage> response) {
                Log.d("DEBUG", response.toString());
                admin.addAll(response);
                r++;
            }

            @Override
            public void handleFault(BackendlessFault fault) {
                Log.d("DEBUG", fault.toString());
            }
        });


    }

    public void populateMessages() {

        forum_query = DataQueryBuilder.create();
        forum_query.setPageSize(80);
        forum_query.setSortBy("created");

        Backendless.Persistence.of(Message.class).find(forum_query, new AsyncCallback<List<Message>>() {
            @Override
            public void handleResponse(final List<Message> response) {
                Log.d("DEBUG", response.toString());
                forum.addAll(response);
                r = 1;

            }

            @Override
            public void handleFault(BackendlessFault fault) {
                Log.d("DEBUG", fault.toString());
                r = 0;
            }
        });
    }

    public void confirmationMail(String file_Name){

        nom_fichier = file_Name;
        AsyncTask.execute(new Runnable() {
            @Override
            public void run() {
                Backendless.Messaging.sendHTMLEmail("Accusé de réception ", getString(R.string.mail_first_part) +"'<strong style='color=red;' color=red>"+ nom_fichier + "'</strong><br/><br/> Etudiant: " + user.getProperty("name").toString() + "<br/> Niveau: "+ user.getProperty("niveau").toString() +"<br/> Domaine: " + user.getProperty("domaine").toString() +"<br/>Vacation: "+ user.getProperty("vacation").toString() + "<br/> ID: "+ user.getUserId()+ "<br/><br/>Cordialement,<br/>" +
                        getString(R.string.mail_second_part), emailList, new AsyncCallback<MessageStatus>() {
                    @Override
                    public void handleResponse(MessageStatus response) {
                        Log.d("DEBUG", response.toString());
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                //Toast.makeText(MainActivity.this, "Mail envoye", Toast.LENGTH_SHORT).show();
                            }
                        });
                    }

                    @Override
                    public void handleFault(BackendlessFault fault) {
                        Log.d("DEBUG", fault.getMessage());
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                //Toast.makeText(MainActivity.this, "Mail non envoye", Toast.LENGTH_SHORT).show();
                            }
                        });
                    }
                });
            }
        });

    }

    public void updateAvatar(Bitmap picture) {
        progressDialog = ProgressDialog.show(this, "", getString(R.string.avatar_start_upload), true);
        Backendless.Files.Android.upload(picture, Bitmap.CompressFormat.PNG, 100, user.getObjectId() + ".png", Defaults.DEFAULT_PATH_AVATAR, true, new AsyncCallback<BackendlessFile>() {
            @Override
            public void handleResponse(BackendlessFile response) {
                Log.d("DEBUG", response.toString());
                Toast.makeText(MainActivity.this, R.string.avatar_succes, Toast.LENGTH_SHORT).show();
                progressDialog.dismiss();
            }

            @Override
            public void handleFault(BackendlessFault fault) {
                Log.d("Debug", fault.getMessage());
                Toast.makeText(MainActivity.this, R.string.avatar_error, Toast.LENGTH_SHORT).show();
                progressDialog.dismiss();
            }
        });



    }



    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
    }


}
