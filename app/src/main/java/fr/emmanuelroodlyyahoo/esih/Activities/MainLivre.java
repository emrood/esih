package fr.emmanuelroodlyyahoo.esih.Activities;

import android.annotation.TargetApi;
import android.app.Fragment;
import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Handler;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.CursorAdapter;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.SearchView;
import android.widget.Toast;

import com.backendless.Backendless;
import com.backendless.async.callback.AsyncCallback;
import com.backendless.exceptions.BackendlessFault;
import com.backendless.persistence.BackendlessDataQuery;
import com.backendless.persistence.DataQueryBuilder;
import com.miguelcatalan.materialsearchview.MaterialSearchView;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import fr.emmanuelroodlyyahoo.esih.Adapters.LivreAdapter;
import fr.emmanuelroodlyyahoo.esih.Fragments.Ressources;
import fr.emmanuelroodlyyahoo.esih.Manifest;
import fr.emmanuelroodlyyahoo.esih.Models.Infos;
import fr.emmanuelroodlyyahoo.esih.Models.Livre;
import fr.emmanuelroodlyyahoo.esih.R;
import fr.emmanuelroodlyyahoo.esih.utils.MonConnecteur;

import static android.Manifest.permission.READ_EXTERNAL_STORAGE;
import static android.Manifest.permission.WRITE_EXTERNAL_STORAGE;
import static com.google.android.gms.internal.zzagz.runOnUiThread;

public class MainLivre extends AppCompatActivity {

    Toolbar toolbarLivre;
    ImageButton ibBackLivre;
    SwipeRefreshLayout swipeLivre;
    SearchView svLivre;
    ListView lvLivre;
    String choice;
    LivreAdapter livreAdapter;
    Livre anLivre;
    ArrayList<Livre> lesLivres;
    ArrayList<Livre> filteredLivre;
    final int PAGESIZE = 80;
    DataQueryBuilder c_query;
    String categorie;
    private Ressources ressources;
    MainActivity mainActivity;
    Intent b;
    String query;
    SearchManager searchManager;
    boolean permission;
    private static final int REQUEST_READ_STORAGE = 0;
    private static final int REQUEST_CAMERA = 0;
    private static final int REQUEST_WRITE_STORAGE = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_livre);
        initViews();

        //Getting data from Fragment ressource
        categorie = getIntent().getStringExtra("choix");
        categorie = getIntent().getExtras().getString("choix");

        //setting request option depending on the cat of book selected
        if (!TextUtils.equals(categorie, "All")) {
            query = "cat1 = " + "'" + categorie + "'";
            c_query.setWhereClause(query);
            Toast.makeText(this, categorie, Toast.LENGTH_SHORT).show();
        } else {
            Toast.makeText(this, "Tous les livres", Toast.LENGTH_SHORT).show(); //must be removed
        }
        c_query.setPageSize(PAGESIZE);

        //Return button
        ibBackLivre.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        //Setting refreshing page logic
        swipeLivre.setRefreshing(true);
        swipeLivre.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                livreAdapter.clear();
                populateLivres();
            }
        });

        //Setting search

        svLivre.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String s) {
                if(!TextUtils.isEmpty(s)){
                    searchItem(s);
                }
                return false;
            }

            @Override
            public boolean onQueryTextChange(String s) {
                return false;
            }
        });


        //Adding books in the list - delay 1s
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                if (isNetworkAvailable()) {
                    if (isOnline()) {
                        if(requestWriteStoragePermission() && requestExternalStoragePermission()){
                            populateLivres();
                        }

                    } else {

                        //message.setVisibility(View.VISIBLE);
                        Snackbar.make(lvLivre, "Pas d'accès à Internet", Snackbar.LENGTH_LONG).setAction("Action", null).show();
                        swipeLivre.setRefreshing(false);
                    }
                } else {
                    //message.setVisibility(View.VISIBLE);
                    Snackbar.make(lvLivre, "Utilisé le Wi-Fi ou les données mobiles", Snackbar.LENGTH_LONG).setAction("Action", null).show();
                    swipeLivre.setRefreshing(false);
                }

            }
        }, 1000);

        //Sending  a book in the list to the reader activity
        lvLivre.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                requestPermission();
                b = new Intent(MainLivre.this, ReaderActivity.class);
                b.putExtra("abook", lesLivres.get(i).getLink());
                b.putExtra("caption", lesLivres.get(i).getTitre());
                Toast.makeText(MainLivre.this, lesLivres.get(i).getTitre(), Toast.LENGTH_SHORT).show();
                startActivity(b);
            }
        });
    }

    //Initialization of all visible objects and more
    public void initViews() {
        toolbarLivre = (Toolbar) findViewById(R.id.toolbarLivre);
        swipeLivre = (SwipeRefreshLayout) findViewById(R.id.swipeLivre);
        lvLivre = (ListView) findViewById(R.id.lvLivre);
        setSupportActionBar(toolbarLivre);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        svLivre = (SearchView) findViewById(R.id.svLivre);
        svLivre.setSubmitButtonEnabled(true);
        ibBackLivre = (ImageButton) findViewById(R.id.ibBackLivre);
        lesLivres = new ArrayList<>();
        filteredLivre = new ArrayList<>();
        livreAdapter = new LivreAdapter(getBaseContext(), lesLivres);
        lvLivre.setAdapter(livreAdapter);
        anLivre = new Livre();
        searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        c_query = DataQueryBuilder.create();
    }

    //Gettings books from the server
    public void populateLivres() {
        //String query = "ID = " + "'" + user.getUserId() + "' and session = 1";
        AsyncTask.execute(new Runnable() {
            @Override
            public void run() {
                Backendless.Persistence.of(Livre.class).find(c_query, new AsyncCallback<List<Livre>>() {
                    @Override
                    public void handleResponse(List<Livre> response) {
                        Log.d("DEBUG", response.toString());
                        lesLivres.addAll(response);
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                livreAdapter.notifyDataSetChanged();
                                swipeLivre.setRefreshing(false);
                            }
                        });
                    }

                    @Override
                    public void handleFault(BackendlessFault fault) {
                        Log.d("DEBUG", fault.getMessage());
                        Toast.makeText(MainLivre.this, "Erreur de connection", Toast.LENGTH_SHORT).show();
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                swipeLivre.setRefreshing(false);
                            }
                        });
                    }
                });
            }
        });
        livreAdapter.notifyDataSetChanged();
    }

    //Check if connection is available
    public Boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnectedOrConnecting();
    }

    //Check if connection is good
    public boolean isOnline() {
        Runtime runtime = Runtime.getRuntime();
        try {
            Process ipProcess = runtime.exec("/system/bin/ping -c 1 8.8.8.8");
            int exitValue = ipProcess.waitFor();
            return (exitValue == 0);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return false;
    }


    @Override
    public void onAttachFragment(Fragment fragment) {
        super.onAttachFragment(fragment);
    }

    @Override
    public void onAttachFragment(android.support.v4.app.Fragment fragment) {
        super.onAttachFragment(fragment);
    }


    public void searchItem(String text) {
        livreAdapter.clear();
        swipeLivre.setRefreshing(true);
        livreAdapter.clear();
        query = "Titre LIKE " + "'%" + text + "%' or auteur Like '%" + text + "%'";
        c_query.setWhereClause(query);
        AsyncTask.execute(new Runnable() {
            @Override
            public void run() {
                Backendless.Persistence.of(Livre.class).find(c_query, new AsyncCallback<List<Livre>>() {
                    @Override
                    public void handleResponse(List<Livre> response) {
                        Log.d("DEBUG", response.toString());
                        lesLivres.addAll(response);
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                livreAdapter.notifyDataSetChanged();
                                swipeLivre.setRefreshing(false);
                            }
                        });
                    }

                    @Override
                    public void handleFault(BackendlessFault fault) {
                        Log.d("DEBUG", fault.getMessage());
                        Toast.makeText(MainLivre.this, "Erreur de connection", Toast.LENGTH_SHORT).show();
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                swipeLivre.setRefreshing(false);
                            }
                        });
                    }
                });
            }
        });
        livreAdapter.notifyDataSetChanged();
    }

    public void requestPermission(){
        if(!requestWriteStoragePermission() && !requestExternalStoragePermission()){
            return;
        }

    }


    public boolean requestExternalStoragePermission() {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
            return true;
        }
        if (checkSelfPermission(READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
            return true;
        }
        if (shouldShowRequestPermissionRationale(READ_EXTERNAL_STORAGE)) {
            Snackbar.make(lvLivre, R.string.permission_rationale, Snackbar.LENGTH_INDEFINITE)
                    .setAction(android.R.string.ok, new View.OnClickListener() {
                        @Override
                        @TargetApi(Build.VERSION_CODES.M)
                        public void onClick(View v) {
                            requestPermissions(new String[]{READ_EXTERNAL_STORAGE}, REQUEST_READ_STORAGE);
                        }
                    });
        } else {
            requestPermissions(new String[]{READ_EXTERNAL_STORAGE}, REQUEST_READ_STORAGE);
        }
        return false;
    }

    public boolean requestWriteStoragePermission() {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
            return true;
        }
        if (checkSelfPermission(WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
            return true;
        }
        if (shouldShowRequestPermissionRationale(WRITE_EXTERNAL_STORAGE)) {
            Snackbar.make(lvLivre, R.string.permission_rationale, Snackbar.LENGTH_INDEFINITE)
                    .setAction(android.R.string.ok, new View.OnClickListener() {
                        @Override
                        @TargetApi(Build.VERSION_CODES.M)
                        public void onClick(View v) {
                            requestPermissions(new String[]{WRITE_EXTERNAL_STORAGE}, REQUEST_WRITE_STORAGE);
                        }
                    });
        } else {
            requestPermissions(new String[]{WRITE_EXTERNAL_STORAGE}, REQUEST_WRITE_STORAGE);
        }
        return false;
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == 0) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                populateLivres();
            } else {
                Toast.makeText(getApplicationContext(), "Nous n'avons pas pu avoir acces a votre appareil", Toast.LENGTH_LONG).show();

            }
        }
    }

    @Override
    public void onBackPressed() {

        super.onBackPressed();

    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
    }

}
