package fr.emmanuelroodlyyahoo.esih.Activities;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;

import com.astuetz.PagerSlidingTabStrip;
import com.backendless.Backendless;
import com.backendless.BackendlessUser;

import org.w3c.dom.Text;

import fr.emmanuelroodlyyahoo.esih.Fragments.Session1;
import fr.emmanuelroodlyyahoo.esih.Fragments.Session2;
import fr.emmanuelroodlyyahoo.esih.R;

public class NotesActivity extends AppCompatActivity {


    String niveau;
    BackendlessUser user;
    Toolbar toolbarNotes;
    TextView tvCaption;
    ImageButton ibBackNotes;
    FragmentManager fm;
    ViewPager notespager;
    TextView tvMoyenne;
    PagerSlidingTabStrip tabsStrip;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notes);
        user = Backendless.UserService.CurrentUser();
        niveau = getIntent().getStringExtra("NIVEAU");
        initViews();
        switch (niveau) {
            case "L1":
                tvCaption.setText("Mes notes de L1");
                break;
            case "L2":
                tvCaption.setText("Mes notes de L2");
                break;
            case "L3":
                tvCaption.setText("Mes notes de L3");
                break;
            case "License":
                tvCaption.setText("Mes notes de Licence");
                break;
            case "M1":
                tvCaption.setText("Mes notes de M1");
                break;
            case "M2":
                tvCaption.setText("Mes notes de M2");
                break;
            default:
                break;
        }

        ibBackNotes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

    }

    public void initViews(){
        //tvMoyenne = (TextView) findViewById(R.id.tvMoyenne);
        notespager = (ViewPager) findViewById(R.id.notespager);
        notespager.setAdapter(new NotespagerAdapter(getSupportFragmentManager()));
        toolbarNotes = (Toolbar) findViewById(R.id.toolbarNotes);
        tvCaption = (TextView) findViewById(R.id.tvCaption);
        ibBackNotes = (ImageButton) findViewById(R.id.ibBackNotes);
        tabsStrip = (PagerSlidingTabStrip) findViewById(R.id.tabs);
        tabsStrip.setViewPager(notespager);
    }

    @Override
    public void onBackPressed() {
        finish();
        super.onBackPressed();
    }


    public  class NotespagerAdapter extends FragmentPagerAdapter {

        final int PAGE_COUNT = 2;
        private String tabTitle[] = {"Session 1", "Session 2"};

        public NotespagerAdapter(FragmentManager fm){
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            if(position == 0){
                return Session1.newScreen(0, niveau);
            }else if (position == 1){
                return  Session2.newScreen(1, niveau);
            }else{
                return  null;
            }
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return tabTitle[position];
        }

        @Override
        public int getCount() {
            return PAGE_COUNT;
        }
    }


    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
    }
}
