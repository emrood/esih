package fr.emmanuelroodlyyahoo.esih.Activities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Environment;
import android.os.Handler;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.backendless.Backendless;
import com.backendless.async.callback.AsyncCallback;
import com.backendless.exceptions.BackendlessFault;
import com.github.barteksc.pdfviewer.PDFView;
import com.github.barteksc.pdfviewer.scroll.DefaultScrollHandle;
import com.github.barteksc.pdfviewer.listener.OnLoadCompleteListener;
import com.github.barteksc.pdfviewer.listener.OnPageChangeListener;
import com.github.barteksc.pdfviewer.scroll.DefaultScrollHandle;
import com.shockwave.pdfium.PdfDocument;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;

import fr.emmanuelroodlyyahoo.esih.Models.Livre;
import fr.emmanuelroodlyyahoo.esih.R;
import fr.emmanuelroodlyyahoo.esih.utils.FileDownloader;


import static com.google.android.gms.internal.zzagz.runOnUiThread;

public class ReaderActivity extends AppCompatActivity {


    PDFView pdfView;
    Toolbar toolbarReader;
    ImageButton ibBackReader;
    String book;
    File pdf;
    ProgressBar pbReader;
    Integer pageNumber = 0;
    String pdfFileName;
    String TAG="PdfActivity";
    int position=-1;
    TextView message;
    TextView tvCaption;
    String caption;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reader);

        pbReader = (ProgressBar) findViewById(R.id.pbReader);
        toolbarReader = (Toolbar) findViewById(R.id.toolbarReader);
        setSupportActionBar(toolbarReader);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        ibBackReader = (ImageButton) findViewById(R.id.ibBackReader);
        tvCaption = (TextView) findViewById(R.id.tvCaption);
        message = (TextView) findViewById(R.id.message);
        pdfView = (PDFView) findViewById(R.id.pdfView);
        book = getIntent().getStringExtra("abook");
        caption = getIntent().getStringExtra("caption");
        pbReader.bringToFront();
        message.setVisibility(View.INVISIBLE);
        pbReader.setVisibility(View.VISIBLE);
        pdfView.setVisibility(View.INVISIBLE);
        String extStorageDirectory = Environment.getExternalStorageDirectory().toString();
        File folder = new File(extStorageDirectory, "ESIH");
        folder.mkdir();



        pdf = new File(folder, "myBook.pdf");

        try{
            tvCaption.setText(caption);
            pdf.createNewFile();
        }catch (IOException e){
            e.printStackTrace();
            tvCaption.setText(" ");
        }

        AsyncTask.execute(new Runnable() {
            @Override
            public void run() {
                if(FileDownloader.downloadFile(book, pdf)){
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Uri path = Uri.fromFile(pdf);
                            try{
                                pdfView.fromFile(pdf).defaultPage(pageNumber).enableSwipe(true).scrollHandle(new DefaultScrollHandle(getBaseContext())).swipeHorizontal(false).scrollHandle(new DefaultScrollHandle(getBaseContext())).load();
                            }catch(Exception e){
                                e.printStackTrace();
                            }

                            pbReader.setVisibility(View.GONE);
                            pdfView.setVisibility(View.VISIBLE);
                        }
                    });


                }else{
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            //Toast.makeText(getBaseContext(), "Erreur de connection", Toast.LENGTH_SHORT).show();
                            Snackbar.make(toolbarReader, "Erreur de connection", Snackbar.LENGTH_LONG).setAction("Action", null).show();
                            pbReader.setVisibility(View.GONE);
                            //pdfView.setVisibility(View.VISIBLE);
                            message.setVisibility(View.VISIBLE);
                        }
                    });

                }

            }
        });

        Uri path = Uri.fromFile(pdf);

        message.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                message.setVisibility(View.INVISIBLE);
                pbReader.setVisibility(View.VISIBLE);
                refreshTask();
            }
        });


        ibBackReader.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                pdf.deleteOnExit();
                pdf.delete();
                finish();
            }
        });

    }

    public void refreshTask(){
        AsyncTask.execute(new Runnable() {
            @Override
            public void run() {
                if(FileDownloader.downloadFile(book, pdf)){
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Uri path = Uri.fromFile(pdf);
                            pdfView.fromFile(pdf).defaultPage(pageNumber).enableSwipe(true).scrollHandle(new DefaultScrollHandle(getBaseContext())).swipeHorizontal(false).scrollHandle(new DefaultScrollHandle(getBaseContext())).load();
                            //Intent intent = new Intent(Intent.ACTION_VIEW);
                            //intent.setDataAndType(Uri.fromFile(pdf), "application/pdf");
                            //intent.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
                            //startActivity(intent);
                            pbReader.setVisibility(View.GONE);
                            pdfView.setVisibility(View.VISIBLE);
                        }
                    });


                }else{
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            //Toast.makeText(getBaseContext(), "Erreur de connection", Toast.LENGTH_SHORT).show();
                            Snackbar.make(toolbarReader, "Erreur de connection", Snackbar.LENGTH_LONG).setAction("Action", null).show();
                            pbReader.setVisibility(View.GONE);
                            //pdfView.setVisibility(View.VISIBLE);
                            message.setVisibility(View.VISIBLE);
                        }
                    });

                }

            }
        });
    }

    @Override
    public void onBackPressed() {
        pdf.deleteOnExit();
        pdf.delete();
        finish();
        super.onBackPressed();
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
    }




}








