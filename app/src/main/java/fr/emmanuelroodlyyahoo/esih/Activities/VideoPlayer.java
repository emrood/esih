package fr.emmanuelroodlyyahoo.esih.Activities;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.media.MediaPlayer;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.widget.MediaController;
import android.widget.VideoView;

import java.io.IOException;

import fr.emmanuelroodlyyahoo.esih.R;

public class VideoPlayer extends AppCompatActivity {

    VideoView detailVideo;
    ProgressDialog pDialog;
    String link = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_video_player);

        detailVideo = (VideoView) findViewById(R.id.detailVideo);

        link = getIntent().getStringExtra("video_link");

        if(!TextUtils.isEmpty(link)){
            loadVideo();
        }else{
            final CharSequence[] options = {"Annulé" };
            AlertDialog.Builder builder = new AlertDialog.Builder(VideoPlayer.this);
            builder.setTitle("ESIH player");
            builder.setMessage("Une erreur est survenue");
            builder.setItems(options, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int item) {
                    if (options[item].equals("Annulé")) {
                        dialog.dismiss();
                        finish();
                    }
                }
            });
            builder.show();
        }


    }

    public void loadVideo(){
        // Create a progressbar
        pDialog = new ProgressDialog(VideoPlayer.this);
        // Set progressbar title
        pDialog.setTitle("ESIH player");
        // Set progressbar message
        pDialog.setMessage("Chargement...");
        pDialog.setIndeterminate(false);
        pDialog.setCancelable(false);
        // Show progressbar
        pDialog.show();

        try {
            // Start the MediaController
            MediaController mediacontroller = new MediaController(
                    VideoPlayer.this);
            mediacontroller.setAnchorView(detailVideo);
            // Get the URL from String VideoURL
            Uri video = Uri.parse(link);
            detailVideo.setMediaController(mediacontroller);
            detailVideo.setVideoURI(video);

        } catch (Exception e) {
            Log.e("Error", e.getMessage());
            e.printStackTrace();
        }

        detailVideo.requestFocus();
        detailVideo.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
            // Close the progress bar and play the video
            public void onPrepared(MediaPlayer mp) {
                pDialog.dismiss();
                detailVideo.start();
            }
        });

    }

    @Override
    public void onBackPressed() {
        finish();
        super.onBackPressed();
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
    }
}
