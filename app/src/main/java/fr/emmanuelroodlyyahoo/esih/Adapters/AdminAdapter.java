package fr.emmanuelroodlyyahoo.esih.Adapters;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Environment;
import android.support.design.widget.Snackbar;
import android.text.Html;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.MediaController;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.VideoView;
import java.io.File;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;

import org.w3c.dom.Text;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import fr.emmanuelroodlyyahoo.esih.Activities.VideoPlayer;
import fr.emmanuelroodlyyahoo.esih.utils.FileDownloader;
import fr.emmanuelroodlyyahoo.esih.Activities.DetailActivity;
import fr.emmanuelroodlyyahoo.esih.Models.AdminMessage;
import fr.emmanuelroodlyyahoo.esih.Models.Livre;
import fr.emmanuelroodlyyahoo.esih.R;
import jp.wasabeef.glide.transformations.RoundedCornersTransformation;

import static android.support.v4.content.ContextCompat.startActivity;
import static com.google.android.gms.internal.zzagz.runOnUiThread;

/**
 * Created by Emmanuel on 13-Dec-17.
 */

public class AdminAdapter extends ArrayAdapter<AdminMessage> {

    public AdminAdapter(Context context, List<AdminMessage> lesMessages) {
        super(context, android.R.layout.simple_list_item_1, lesMessages);
    }


    public static class ViewHolder{
        TextView tvTitre;
        TextView tvdescription;
        TextView tvDateRemise;

    }


    private AdminMessage amessage;


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        amessage = getItem(position);
        AdminAdapter.ViewHolder viewHolder;


        if(convertView == null){
            LayoutInflater inflater = LayoutInflater.from(getContext());
            convertView = inflater.inflate(R.layout.admin_message, parent, false);
            viewHolder = new AdminAdapter.ViewHolder();
            viewHolder.tvTitre = (TextView) convertView.findViewById(R.id.tvTitre);
            viewHolder.tvdescription = (TextView) convertView.findViewById(R.id.tvDesciption);
            viewHolder.tvDateRemise = (TextView) convertView.findViewById(R.id.tvDateRemise);


            convertView.setTag(viewHolder);

        }else{
            viewHolder = (AdminAdapter.ViewHolder) convertView.getTag();
        }

        viewHolder.tvDateRemise.setVisibility(View.GONE);

        if(!TextUtils.isEmpty(amessage.getDateRemise())){
            viewHolder.tvDateRemise.setText(amessage.getDateRemise());
            viewHolder.tvDateRemise.setVisibility(View.VISIBLE);
        }

        viewHolder.tvTitre.setText(amessage.getTitle());
        viewHolder.tvdescription.setText(Html.fromHtml(amessage.getDetail()));



        return convertView;
    }




}
