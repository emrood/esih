package fr.emmanuelroodlyyahoo.esih.Adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.List;

import fr.emmanuelroodlyyahoo.esih.Models.Horaire;
import fr.emmanuelroodlyyahoo.esih.R;

/**
 * Created by Emmanuel on 10/2/2017.
 */

public class HoraireAdapter extends ArrayAdapter<Horaire> {

    public HoraireAdapter(Context context, List<Horaire> horaires){
        super(context, android.R.layout.simple_list_item_1, horaires);
    }

    public static class ViewHolder{


    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        Horaire horaire = this.getItem(position);
        ViewHolder viewHolder;

        if(convertView == null){
            LayoutInflater inflater = LayoutInflater.from(getContext());
            convertView = inflater.inflate(R.layout.item_horaire, parent, false);
            viewHolder = new ViewHolder();


            convertView.setTag(viewHolder);

        }else{
            viewHolder = (ViewHolder) convertView.getTag();
        }

        return convertView;
    }

}
