package fr.emmanuelroodlyyahoo.esih.Adapters;


import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;


import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;

import java.util.List;

import fr.emmanuelroodlyyahoo.esih.Models.Infos;
import fr.emmanuelroodlyyahoo.esih.R;
import jp.wasabeef.glide.transformations.RoundedCornersTransformation;


public class InfoAdapter extends ArrayAdapter<Infos> {

    public InfoAdapter(Context context, List<Infos> lesinfos) {
        super(context, android.R.layout.simple_list_item_1, lesinfos);
    }


    public static class ViewHolder{
        TextView tvTitre;
        TextView tvDescription;
    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        final Infos aInfo = getItem(position);
        ViewHolder viewHolder;

        if(convertView == null){
            LayoutInflater inflater = LayoutInflater.from(getContext());
            convertView = inflater.inflate(R.layout.item_info, parent, false);
            viewHolder = new ViewHolder();
            viewHolder.tvTitre = (TextView) convertView.findViewById(R.id.tvTitle);

            viewHolder.tvDescription = (TextView) convertView.findViewById(R.id.tvDescription);
            convertView.setTag(viewHolder);

        }else{
            viewHolder = (ViewHolder) convertView.getTag();
        }

        ImageView ivthumbnail = (ImageView) convertView.findViewById(R.id.thumbnail);

        //Mise en place des information dans les objets view
        viewHolder.tvTitre.setText(aInfo.getTitre());
        viewHolder.tvDescription.setText(aInfo.getDescription());

        Glide.with(getContext()).load(aInfo.getPath()).placeholder(R.drawable.logo2).fitCenter().error(R.drawable.logo2).into(ivthumbnail);



        return convertView;
    }
}
