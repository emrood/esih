package fr.emmanuelroodlyyahoo.esih.Adapters;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Environment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;

import java.io.File;
import java.io.IOException;
import java.util.List;

import fr.emmanuelroodlyyahoo.esih.Models.Infos;
import fr.emmanuelroodlyyahoo.esih.Models.Livre;
import fr.emmanuelroodlyyahoo.esih.R;
import fr.emmanuelroodlyyahoo.esih.utils.FileDownloader;

import static com.google.android.gms.internal.zzagz.runOnUiThread;

/**
 * Created by Emmanuel on 24-Nov-17.
 */

public class LivreAdapter extends ArrayAdapter<Livre> {

    public LivreAdapter(Context context, List<Livre> lesLivres) {
        super(context, android.R.layout.simple_list_item_1, lesLivres);
    }


    public static class ViewHolder{
        TextView tvTitre;
        TextView tvAuteur;
    }


    File pdf;
    File file;



    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        final Livre alivre = getItem(position);
        LivreAdapter.ViewHolder viewHolder;

        if(convertView == null){
            LayoutInflater inflater = LayoutInflater.from(getContext());
            convertView = inflater.inflate(R.layout.item_livre, parent, false);
            viewHolder = new LivreAdapter.ViewHolder();
            viewHolder.tvTitre = (TextView) convertView.findViewById(R.id.tvTitreLivre);
            viewHolder.tvAuteur = (TextView) convertView.findViewById(R.id.tvAuteur);
            convertView.setTag(viewHolder);

        }else{
            viewHolder = (LivreAdapter.ViewHolder) convertView.getTag();
        }



        //Mise en place des information dans les objets view
        viewHolder.tvTitre.setText(alivre.getTitre());
        viewHolder.tvAuteur.setText(alivre.getAuteur());





        return convertView;
    }


}
