package fr.emmanuelroodlyyahoo.esih.Adapters;

import android.app.Application;
import android.content.Context;
import android.graphics.Color;
import android.support.v7.util.DiffUtil;
import android.support.v7.widget.CardView;
import android.text.TextUtils;
import android.text.format.DateUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import android.widget.ImageView;

import com.backendless.Backendless;
import com.backendless.BackendlessUser;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import fr.emmanuelroodlyyahoo.esih.Fragments.Ressources;
import fr.emmanuelroodlyyahoo.esih.Models.Message;
import fr.emmanuelroodlyyahoo.esih.Models.Note;
import fr.emmanuelroodlyyahoo.esih.R;
import fr.emmanuelroodlyyahoo.esih.utils.MessageDiffCallback;
import jp.wasabeef.glide.transformations.CropCircleTransformation;
import jp.wasabeef.glide.transformations.RoundedCornersTransformation;

/**
 * Created by Emmanuel on 10/19/2017.
 */

public class MessageAdapter extends ArrayAdapter<Message> {

    public MessageAdapter(Context context, List<Message> lesMessages) {
        super(context, android.R.layout.simple_list_item_1, lesMessages);

    }

    public static class ViewHolder {
        TextView tvSender;
        TextView tvMessageBody;
        TextView tvMessageDate;

    }

    BackendlessUser user;
    CardView cardMessage;
    Context ctx;
    ImageView ivUserAvatar;


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final Message aMessage = getItem(position);
        ViewHolder viewHolder;
        ctx = getContext();
        user = Backendless.UserService.CurrentUser();

        if (convertView == null) {
            LayoutInflater inflater = LayoutInflater.from(getContext());


            convertView = inflater.inflate(R.layout.item_message, parent, false);

            viewHolder = new ViewHolder();

            viewHolder.tvSender = (TextView) convertView.findViewById(R.id.tvSender);
            viewHolder.tvMessageBody = (TextView) convertView.findViewById(R.id.tvMessageBody);
            viewHolder.tvMessageDate = (TextView) convertView.findViewById(R.id.tvMessageDate);
            cardMessage = (CardView) convertView.findViewById(R.id.cardMessage);
            ivUserAvatar = (ImageView) convertView.findViewById(R.id.ivUserAvatar);
            convertView.setTag(viewHolder);

        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        //Mise en place des information dans les objets view

        viewHolder.tvSender.setText(aMessage.getSender());
        viewHolder.tvMessageBody.setText(aMessage.getBody());
        viewHolder.tvMessageDate.setText(getRelativeTimeAgo(aMessage.getCreated()));



        Glide.with(getContext()).load(R.drawable.user).bitmapTransform(new RoundedCornersTransformation(getContext(), 30, 5), new CropCircleTransformation(getContext())).centerCrop()
                .placeholder(R.drawable.user).error(R.drawable.user).into(ivUserAvatar);


        return convertView;
    }



    public String getRelativeTimeAgo(String rawJsonDate) {
        String backendFormat = "MM/dd/yyyy HH:mm:ss";
        SimpleDateFormat sf = new SimpleDateFormat(backendFormat, Locale.ENGLISH);
        sf.setLenient(true);

        String relativeDate = "";
        try {
            long dateMillis = sf.parse(rawJsonDate).getTime();
            relativeDate = DateUtils.getRelativeTimeSpanString(dateMillis,
                    System.currentTimeMillis(), DateUtils.SECOND_IN_MILLIS).toString();
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return relativeDate;
    }

}
