package fr.emmanuelroodlyyahoo.esih.Adapters;

import android.content.Context;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import fr.emmanuelroodlyyahoo.esih.Models.Note;
import fr.emmanuelroodlyyahoo.esih.R;

/**
 * Created by Emmanuel on 9/27/2017.
 */

public class NotesAdapter extends ArrayAdapter<Note> {

    public NotesAdapter(Context context, List<Note> lesnotes) {
        super(context, android.R.layout.simple_list_item_1, lesnotes);
    }

    public static class ViewHolder{
        TextView tvMatiere;
        TextView tvIntra;
        TextView tvFinal;
        TextView tvTotal;
        TextView tvDevoir;
    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        final Note aNote = getItem(position);
        ViewHolder viewHolder;

        if(convertView == null){
            LayoutInflater inflater = LayoutInflater.from(getContext());
            convertView = inflater.inflate(R.layout.item_note, parent, false);
            viewHolder = new ViewHolder();
            viewHolder.tvMatiere = (TextView) convertView.findViewById(R.id.tvMatiere);
            viewHolder.tvFinal = (TextView) convertView.findViewById(R.id.tvFinal);
            viewHolder.tvDevoir = (TextView) convertView.findViewById(R.id.tvDevoir);
            viewHolder.tvIntra = (TextView) convertView.findViewById(R.id.tvIntra);
            viewHolder.tvTotal = (TextView) convertView.findViewById(R.id.tvTotal);

            convertView.setTag(viewHolder);

        }else{
            viewHolder = (ViewHolder) convertView.getTag();
        }

        //Mise en place des information dans les objets view

        viewHolder.tvMatiere.setText(aNote.getMatiere());
        viewHolder.tvFinal.setText("final: " + aNote.getNfinal() + "/100");
        viewHolder.tvTotal.setText("Total: " + aNote.getNtotal()+ "/100");
        viewHolder.tvDevoir.setText("devoir: " + aNote.getDevoir()+ "/10");
        viewHolder.tvIntra.setText("Intra: " + aNote.getNintra()+ "/100");





        return convertView;
    }

}
