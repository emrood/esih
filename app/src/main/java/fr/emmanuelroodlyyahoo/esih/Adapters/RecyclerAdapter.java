package fr.emmanuelroodlyyahoo.esih.Adapters;

import android.content.Context;
import android.support.v7.util.DiffUtil;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.text.format.DateUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.backendless.Backendless;
import com.backendless.BackendlessUser;
import com.bumptech.glide.Glide;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Locale;

import fr.emmanuelroodlyyahoo.esih.Models.Message;
import fr.emmanuelroodlyyahoo.esih.R;
import fr.emmanuelroodlyyahoo.esih.utils.MessageDiffCallback;
import jp.wasabeef.glide.transformations.CropCircleTransformation;
import jp.wasabeef.glide.transformations.RoundedCornersTransformation;

/**
 * Created by Emmanuel on 2/2/2018.
 */

public class RecyclerAdapter extends RecyclerView.Adapter<RecyclerAdapter.ViewHolder> {


    BackendlessUser user;
    CardView cardMessage;
    Context ctx;
    ImageView ivUserAvatar;



    ArrayList<Message> mesMessages;

    public RecyclerAdapter(Context ctx, ArrayList<Message> mMessages) {
        this.ctx = ctx;
        this.mesMessages = mMessages;
    }

    public Context getCtx() {
        return ctx;
    }

    @Override
    public RecyclerAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        Context context = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);

        //Inflate the custom layout
        View messageView = inflater.inflate(R.layout.item_message, parent, false);

        //Return a new holder instance
        ViewHolder viewHolder = new ViewHolder(messageView);
        user = Backendless.UserService.CurrentUser();

        return viewHolder;

    }



    @Override
    public int getItemCount() {
        return mesMessages.size();
    }



    public class ViewHolder extends RecyclerView.ViewHolder {

        public TextView tvSender;
        public TextView tvMessageBody;
        public TextView tvMessageDate;


        public ViewHolder(View itemView) {
            // Stores the itemView in a public final member variable that can be used
            // to access the context from any ViewHolder instance.
            super(itemView);
            tvSender = (TextView) itemView.findViewById(R.id.tvSender);
            tvMessageBody = (TextView) itemView.findViewById(R.id.tvMessageBody);
            tvMessageDate = (TextView) itemView.findViewById(R.id.tvMessageDate);
            cardMessage = (CardView) itemView.findViewById(R.id.cardMessage);
            ivUserAvatar = (ImageView) itemView.findViewById(R.id.ivUserAvatar);
        }
    }

    @Override
    public void onBindViewHolder(RecyclerAdapter.ViewHolder holder, int position) {

        TextView sender = holder.tvSender;
        TextView body = holder.tvMessageBody;
        TextView date = holder.tvMessageDate;

        Message aMessage = mesMessages.get(position);
        // Set item views based on your views and data model
        sender.setText(aMessage.getSender());
        body.setText(aMessage.getBody());
        date.setText(getRelativeTimeAgo(aMessage.getCreated()));

        Glide.with(getCtx()).load(user.getProperty("avatar")).bitmapTransform(new RoundedCornersTransformation(getCtx(), 30, 5), new CropCircleTransformation(getCtx())).centerCrop()
                .placeholder(R.drawable.user).error(R.drawable.user).into(ivUserAvatar);

    }

    public void swapItems(ArrayList<Message>  messages) {
        // compute diffs
        final MessageDiffCallback diffCallback = new MessageDiffCallback(this.mesMessages, messages);
        final DiffUtil.DiffResult diffResult = DiffUtil.calculateDiff(diffCallback);

        // clear contacts and add
        this.mesMessages.clear();
        this.mesMessages.addAll(messages);

        diffResult.dispatchUpdatesTo(this); // calls adapter's notify methods after diff is computed
    }



    public String getRelativeTimeAgo(String rawJsonDate) {
        String backendFormat = "MM/dd/yyyy HH:mm:ss";
        SimpleDateFormat sf = new SimpleDateFormat(backendFormat, Locale.ENGLISH);
        sf.setLenient(true);

        String relativeDate = "";
        try {
            long dateMillis = sf.parse(rawJsonDate).getTime();
            relativeDate = DateUtils.getRelativeTimeSpanString(dateMillis,
                    System.currentTimeMillis(), DateUtils.SECOND_IN_MILLIS).toString();
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return relativeDate;
    }

}
