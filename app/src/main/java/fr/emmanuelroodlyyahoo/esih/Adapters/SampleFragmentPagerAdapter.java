package fr.emmanuelroodlyyahoo.esih.Adapters;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import fr.emmanuelroodlyyahoo.esih.Fragments.Session1;
import fr.emmanuelroodlyyahoo.esih.Fragments.Session2;

/**
 * Created by Emmanuel on 9/26/2017.
 */

public class SampleFragmentPagerAdapter extends FragmentPagerAdapter {
    final int PAGE_COUNT = 2;
    private String tabTitles[] = new String[] { "Session I", "Session II"};

    public SampleFragmentPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public int getCount() {
        return PAGE_COUNT;
    }

    @Override
    public Fragment getItem(int position) {
        if(position == 0){
            return Session1.newInstance(position + 1);
        }else if (position == 1){
            return  Session2.newInstance(position + 1);
        }else{
            return  null;
        }
    }

    @Override
    public CharSequence getPageTitle(int position) {
        // Generate title based on item position
        return tabTitles[position];
    }
}
