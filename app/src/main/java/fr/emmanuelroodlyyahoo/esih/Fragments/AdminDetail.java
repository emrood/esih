package fr.emmanuelroodlyyahoo.esih.Fragments;


import android.content.Intent;
import android.os.Build;
import android.support.v4.app.DialogFragment;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.Nullable;
import android.text.Html;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.backendless.Backendless;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;

import java.io.File;
import java.io.IOException;

import fr.emmanuelroodlyyahoo.esih.Activities.MainActivity;
import fr.emmanuelroodlyyahoo.esih.Activities.VideoPlayer;
import fr.emmanuelroodlyyahoo.esih.Models.AdminMessage;
import fr.emmanuelroodlyyahoo.esih.R;
import fr.emmanuelroodlyyahoo.esih.utils.Defaults;
import fr.emmanuelroodlyyahoo.esih.utils.FileDownloader;

import static com.google.android.gms.internal.zzagz.runOnUiThread;

/**
 * Created by Emmanuel on 3/5/2018.
 */

public class AdminDetail extends DialogFragment {


    TextView tvTitle;
    TextView tvDetail;
    Button download;
    ImageView ivMessageImage;
    ImageView ibCancel;
    private AdminMessage amessage;
    File pdf;
    File file;
    View root;
    Button btn_play;


    static AdminDetail newInstance(AdminMessage adm){
        AdminDetail f = new AdminDetail();

        Bundle args = new Bundle();
        args.putSerializable("adm_message", adm);
        f.setArguments(args);
        return f;
    }




    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        root = inflater.inflate(R.layout.detail_fragment, null);

        initViews();

        amessage = (AdminMessage) getArguments().getSerializable("adm_message");

        tvTitle.setText(amessage.getTitle());
        tvDetail.setText(Html.fromHtml(amessage.getDetail()));


        if(!TextUtils.isEmpty(amessage.getDateRemise())){

        }

        if(!TextUtils.isEmpty(amessage.getVideo())){

            btn_play.setVisibility(View.VISIBLE);

        }

        if(!TextUtils.isEmpty(amessage.getImg())){
            ivMessageImage.setVisibility(View.VISIBLE);
            loadImage();
        }

        if(!TextUtils.isEmpty(amessage.getLink())) {

            download.setVisibility(View.VISIBLE);
            String extStorageDirectory = Environment.getExternalStorageDirectory().toString();
            File file = new File(extStorageDirectory, "ESIH");
            file.mkdirs();
            pdf = new File(file, amessage.getFile_name() + ".pdf");

            try {
                pdf.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }

            download.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    AsyncTask.execute(new Runnable() {
                        @Override
                        public void run() {

                            if (FileDownloader.downloadFile(amessage.getLink(), pdf)) {
                                runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        if(Build.VERSION.SDK_INT <= Build.VERSION_CODES.LOLLIPOP){
                                            Toast.makeText(getContext(), "Téléchargement...", Toast.LENGTH_SHORT).show();
                                        }else{
                                            Toast.makeText(getActivity(), "Téléchargement...", Toast.LENGTH_SHORT).show();
                                        }

                                    }
                                });

                            } else {
                                runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        if(Build.VERSION.SDK_INT <= Build.VERSION_CODES.LOLLIPOP){
                                            Toast.makeText(getContext(), "Erreur lors du téléchargement", Toast.LENGTH_SHORT).show();
                                        }else{
                                            Toast.makeText(getActivity(), "Erreur lors du téléchargement", Toast.LENGTH_SHORT).show();
                                        }

                                    }
                                });

                            }
                        }
                    });
                }
            });
        }

        btn_play.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(getActivity(), VideoPlayer.class);
                i.putExtra("video_link", amessage.getVideo());
                getActivity().startActivity(i);
            }
        });

        ibCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dismiss();
            }
        });

        return root;
    }


    public void initViews(){
        tvTitle = (TextView) root.findViewById(R.id.tvTitle);
        tvDetail = (TextView) root.findViewById(R.id.tvDetail);
        download = (Button) root.findViewById(R.id.downloadFile);
        ivMessageImage = (ImageView) root.findViewById(R.id.ivMessageImage);
        ibCancel = (ImageView) root.findViewById(R.id.ibCancel2);
        btn_play = (Button) root.findViewById(R.id.btn_play);
        btn_play.setVisibility(View.GONE);
        btn_play.setVisibility(View.INVISIBLE);
        ivMessageImage.setVisibility(View.GONE);
        download.setVisibility(View.GONE);
        download.setVisibility(View.INVISIBLE);
    }

    public void loadImage(){
        ivMessageImage.setVisibility(View.VISIBLE);
        Glide.with(getActivity())
                .load(amessage.getImg())
                .listener(new RequestListener<String, GlideDrawable>() {
                    @Override
                    public boolean onException(Exception e, String model, Target<GlideDrawable> target, boolean isFirstResource) {
                        ivMessageImage.setVisibility(View.GONE);
                        return false; // important to return false so the error placeholder can be placed
                    }

                    @Override
                    public boolean onResourceReady(GlideDrawable resource, String model, Target<GlideDrawable> target, boolean isFromMemoryCache, boolean isFirstResource) {

                        return false;
                    }
                })
                .placeholder(R.drawable.logo2).error(R.drawable.logo2).into(ivMessageImage);

    }


    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
    }

}
