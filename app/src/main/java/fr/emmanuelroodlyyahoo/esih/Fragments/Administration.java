package fr.emmanuelroodlyyahoo.esih.Fragments;

import android.annotation.TargetApi;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.DialogFragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import com.backendless.Backendless;
import com.backendless.BackendlessUser;
import com.backendless.async.callback.AsyncCallback;
import com.backendless.exceptions.BackendlessFault;
import com.backendless.persistence.DataQueryBuilder;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import fr.emmanuelroodlyyahoo.esih.Activities.MainActivity;
import fr.emmanuelroodlyyahoo.esih.Activities.VideoPlayer;
import fr.emmanuelroodlyyahoo.esih.Adapters.AdminAdapter;
import fr.emmanuelroodlyyahoo.esih.Models.AdminMessage;
import fr.emmanuelroodlyyahoo.esih.R;

import static android.Manifest.permission.READ_EXTERNAL_STORAGE;
import static android.Manifest.permission.WRITE_EXTERNAL_STORAGE;

/**
 * Created by Emmanuel on 12-Dec-17.
 */

public class Administration extends Fragment{



    BackendlessUser user;
    View view;
    DateFormat df;
    MainActivity mainActivity;
    SwipeRefreshLayout swipeAdmin;
    TextView message;
    ListView lvAdmin;
    ArrayList<AdminMessage> adminMessages;
    AdminAdapter adminAdapter;
    DataQueryBuilder c_query;
    final int PAGESIZE = 25;
    Bundle args;
    FragmentManager fm;
    private static final int REQUEST_READ_STORAGE = 0;
    private static final int REQUEST_CAMERA = 0;
    private static final int REQUEST_WRITE_STORAGE = 0;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.administration_ui, null, false);
        user = Backendless.UserService.CurrentUser();
        df = new SimpleDateFormat("MM/dd/yyyy");
        mainActivity = (MainActivity) getActivity();
        adminMessages = new ArrayList<>();
        adminAdapter = new AdminAdapter(getContext(), mainActivity.admin);
        message = (TextView) view.findViewById(R.id.message);
        InitViews();
        args = new Bundle();
        fm = getFragmentManager();
        lvAdmin.setAdapter(adminAdapter);
        c_query = DataQueryBuilder.create();
        c_query.setPageSize(PAGESIZE);
        c_query.setSortBy("created");
        String query = "niveau = " + "'" + user.getProperty("niveau") + "'";
        c_query.setWhereClause(query);
        adminAdapter.notifyDataSetChanged();

        message.setVisibility(View.INVISIBLE);

        if(mainActivity.admin.isEmpty()){
            message.setVisibility(View.VISIBLE);
        }


        lvAdmin.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                    requestPermission();
                    args.putSerializable("adm_message",mainActivity.admin.get(i) );
                    AdminDetail amd = AdminDetail.newInstance(mainActivity.admin.get(i));
                    amd.show(fm,"TAG");
            }
        });


        swipeAdmin.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                adminAdapter.clear();
                mainActivity.admin.clear();
                mainActivity.populateAdminMessages();
                adminAdapter.notifyDataSetChanged();
                swipeAdmin.setRefreshing(false);
                populateAdminMessages();
            }
        });

        return view;
    }

    private void InitViews() {
        swipeAdmin = (SwipeRefreshLayout) view.findViewById(R.id.swipeAdmin);
        lvAdmin = (ListView) view.findViewById(R.id.lvAdmin);
    }

    private void populateAdminMessages() {

        Backendless.Persistence.of(AdminMessage.class).find(c_query, new AsyncCallback<List<AdminMessage>>() {
            @Override
            public void handleResponse(List<AdminMessage> response) {
                Log.d("DEBUG", response.toString());
                if(response.isEmpty()){
                    message.setVisibility(View.VISIBLE);
                }else{
                    message.setVisibility(View.INVISIBLE);
                }
                adminAdapter.addAll(response);
                adminAdapter.notifyDataSetChanged();
                swipeAdmin.setRefreshing(false);
            }

            @Override
            public void handleFault(BackendlessFault fault) {
                Log.d("DEBUG", fault.toString());
                swipeAdmin.setRefreshing(false);
            }
        });

        adminAdapter.notifyDataSetChanged();

    }

    public void requestPermission(){
        if(!requestWriteStoragePermission() && !requestExternalStoragePermission()){
            return;
        }

    }

    public boolean requestExternalStoragePermission() {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
            return true;
        }
        if (getActivity().checkSelfPermission(READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
            return true;
        }
        if (shouldShowRequestPermissionRationale(READ_EXTERNAL_STORAGE)) {
            Snackbar.make(lvAdmin, R.string.permission_rationale, Snackbar.LENGTH_INDEFINITE)
                    .setAction(android.R.string.ok, new View.OnClickListener() {
                        @Override
                        @TargetApi(Build.VERSION_CODES.M)
                        public void onClick(View v) {
                            requestPermissions(new String[]{READ_EXTERNAL_STORAGE}, REQUEST_READ_STORAGE);
                        }
                    });
        } else {
            requestPermissions(new String[]{READ_EXTERNAL_STORAGE}, REQUEST_READ_STORAGE);
        }
        return false;
    }

    public boolean requestWriteStoragePermission() {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
            return true;
        }
        if (getActivity().checkSelfPermission(WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
            return true;
        }
        if (shouldShowRequestPermissionRationale(WRITE_EXTERNAL_STORAGE)) {
            Snackbar.make(lvAdmin, R.string.permission_rationale, Snackbar.LENGTH_INDEFINITE)
                    .setAction(android.R.string.ok, new View.OnClickListener() {
                        @Override
                        @TargetApi(Build.VERSION_CODES.M)
                        public void onClick(View v) {
                            requestPermissions(new String[]{WRITE_EXTERNAL_STORAGE}, REQUEST_WRITE_STORAGE);
                        }
                    });
        } else {
            requestPermissions(new String[]{WRITE_EXTERNAL_STORAGE}, REQUEST_WRITE_STORAGE);
        }
        return false;
    }




}
