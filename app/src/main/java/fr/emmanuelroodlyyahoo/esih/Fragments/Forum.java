package fr.emmanuelroodlyyahoo.esih.Fragments;

import android.app.NotificationManager;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.NotificationCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.util.DiffUtil;
import android.support.v7.util.ListUpdateCallback;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.backendless.Backendless;
import com.backendless.BackendlessUser;
import com.backendless.Subscription;
import com.backendless.async.callback.AsyncCallback;
import com.backendless.exceptions.BackendlessFault;
import com.backendless.messaging.DeliveryOptions;
import com.backendless.messaging.MessageStatus;
import com.backendless.messaging.PublishOptions;
import com.backendless.messaging.PublishPolicyEnum;
import com.backendless.messaging.PublishStatusEnum;
import com.backendless.messaging.PushBroadcastMask;
import com.backendless.messaging.SubscriptionOptions;
import com.backendless.persistence.DataQueryBuilder;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import fr.emmanuelroodlyyahoo.esih.Activities.MainActivity;
import fr.emmanuelroodlyyahoo.esih.Adapters.MessageAdapter;
import fr.emmanuelroodlyyahoo.esih.Adapters.NotesAdapter;
import fr.emmanuelroodlyyahoo.esih.Adapters.RecyclerAdapter;
import fr.emmanuelroodlyyahoo.esih.Models.Message;
import fr.emmanuelroodlyyahoo.esih.Models.Note;
import fr.emmanuelroodlyyahoo.esih.R;
import fr.emmanuelroodlyyahoo.esih.utils.MessageDiffCallback;

import static com.google.android.gms.internal.zzagz.runOnUiThread;

/**
 * Created by Emmanuel Roodly on 18/09/2017.
 */

public class Forum extends Fragment {

    BackendlessUser user;

    public static final String ARG_PAGE = "ARG_PAGE";

    private int mPage;
    ListView lvForum;
    ArrayList<Message> mesMessages;
    MessageAdapter messageAdapter;
    SwipeRefreshLayout swipeForum;
    DataQueryBuilder c_query;
    EditText etMessageSend;
    Button btnSendMessage;
    TextView message;
    MainActivity mainActivity;
    DeliveryOptions deliveryOptions;
    final int PAGESIZE = 80;
    CountDownTimer countDownTimer;
    public Handler h;
    public int listSize;
    public String myavatar;
    private SubscriptionOptions subscriptionOptions;
    private Subscription subscription;
    private PublishOptions publishOptions;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.forum_ui, container, false);
        user = Backendless.UserService.CurrentUser();
        //Initialiation and setting views
        lvForum = (ListView) view.findViewById(R.id.lvForum);

        //rvMessage = (RecyclerView) view.findViewById(R.id.rvMessage);
        mesMessages = new ArrayList<>();

        //adapter = new RecyclerAdapter(getContext(), mesMessages);

        btnSendMessage = (Button) view.findViewById(R.id.btnSendMessage);
        etMessageSend = (EditText) view.findViewById(R.id.etSendMessage);
        message = (TextView) view.findViewById(R.id.message);
        //rvMessage.setAdapter(adapter);

        //rvMessage.setLayoutManager(new LinearLayoutManager(getContext()));
        message.setVisibility(View.INVISIBLE);
        mainActivity = (MainActivity) getActivity();
        mainActivity.fab.setVisibility(View.GONE);
        messageAdapter = new MessageAdapter(getContext(), mainActivity.forum);
        messageAdapter.notifyDataSetChanged();
        swipeForum = (SwipeRefreshLayout) view.findViewById(R.id.swipeForum);
        lvForum.setAdapter(messageAdapter);


        publishOptions = new PublishOptions();
        publishOptions.setPublisherId( user.getProperty("name").toString());
        publishOptions.setSubtopic("Forum");

        subscriptionOptions = new SubscriptionOptions();
        subscriptionOptions.setSubtopic("Forum");

        //Setting server request preferences
        c_query = DataQueryBuilder.create();
        c_query.setPageSize(PAGESIZE);
        c_query.setSortBy("created");

        if(mainActivity.forum.isEmpty()){
            swipeForum.setRefreshing(true);
            messageAdapter.clear();
            mainActivity.forum.clear();
            mainActivity.populateMessages();
            messageAdapter.notifyDataSetChanged();
        }

        /*
        //Request messages from server
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                if (isNetworkAvailable()) {
                    populateMessages();
                } else {
                    //message.setVisibility(View.VISIBLE); 
                    Snackbar.make(lvForum, "Utilisé le Wi-Fi ou les données mobiles", Snackbar.LENGTH_LONG).setAction("Action", null).show();
                    swipeForum.setRefreshing(false);
                }
            }
        }, 2000);*/


        //Send message to server
        btnSendMessage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!TextUtils.isEmpty(etMessageSend.getText())) {
                    SendMessage(etMessageSend.getText().toString());
                    etMessageSend.clearFocus();
                    etMessageSend.setText("");
                }
            }
        });

        swipeForum.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                messageAdapter.clear();
                message.setVisibility(View.INVISIBLE);
                mainActivity.populateMessages();
                messageAdapter.notifyDataSetChanged();
                swipeForum.setRefreshing(false);
            }
        });


        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                //backgroundRefreshTask();
                try{
                    backgroundUpdate();
                }catch (Exception e){
                   e.printStackTrace();
                }
            }
        }, 5000);

        etMessageSend.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View view, int i, KeyEvent keyEvent) {
                if (!TextUtils.isEmpty(etMessageSend.getText())) {
                    SendMessage(etMessageSend.getText().toString());
                    etMessageSend.clearFocus();
                    etMessageSend.setText("");
                }
                return false;
            }
        });


        return view;
    }

    public void populateMessages() {
        AsyncTask.execute(new Runnable() {
            @Override
            public void run() {
                Backendless.Persistence.of(Message.class).find(c_query, new AsyncCallback<List<Message>>() {
                    @Override
                    public void handleResponse(final List<Message> response) {
                        Log.d("DEBUG", response.toString());
                        mesMessages.addAll(response);

                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                messageAdapter.notifyDataSetChanged();
                                swipeForum.setRefreshing(false);
                            }
                        });
                    }

                    @Override
                    public void handleFault(BackendlessFault fault) {
                        Log.d("DEBUG", fault.toString());
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                swipeForum.setRefreshing(false);
                                message.setVisibility(View.VISIBLE);
                            }
                        });

                    }
                });

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        messageAdapter.notifyDataSetChanged();
                        listSize = messageAdapter.getCount();
                        lvForum.smoothScrollToPosition(mesMessages.size() - 1);
                    }
                });
            }
        });
    }


    public void pushNotification(String sender, String body) {

        publishOptions = new PublishOptions();

        publishOptions.putHeader("android-ticker-text", "ESIH");
        publishOptions.putHeader("android-content-title", sender + " a publie un nouveau message");
        publishOptions.putHeader("android-content-text", body);

        deliveryOptions = new DeliveryOptions();
        deliveryOptions.setPublishPolicy(PublishPolicyEnum.BOTH);
        deliveryOptions.setPushBroadcast( PushBroadcastMask.ANDROID | PushBroadcastMask.IOS );

        Backendless.Messaging.publish(user.getProperty("name").toString() + " a publie un nouveau message", publishOptions, deliveryOptions, new AsyncCallback<MessageStatus>() {
            @Override
            public void handleResponse(MessageStatus response) {
                Log.d("DEBUG", response.toString());
            }

            @Override
            public void handleFault(BackendlessFault fault) {
                Log.d("DEBUG", fault.toString());
            }
        });

    }

    public void SendMessage(String message) {

        final Message ms = new Message(user.getProperty("name").toString(), message, user.getProperty("niveau").toString());
        Backendless.Persistence.of(Message.class).save(ms, new AsyncCallback<Message>() {
            @Override
            public void handleResponse(Message response) {
                Log.d("DEBUG", response.toString());
                mainActivity.forum.add(ms);
                messageAdapter.notifyDataSetChanged();

                lvForum.smoothScrollToPosition(mainActivity.forum.size() - 1);
                pushNotification(user.getProperty("name").toString(), ms.getBody());
            }

            @Override
            public void handleFault(BackendlessFault fault) {
                Log.d("DEBUG", fault.toString());
                Toast.makeText(mainActivity, "erreur", Toast.LENGTH_SHORT).show();
            }
        });

    }




    public void swapItems(List<Message> messages) {
        // compute diffs
        final MessageDiffCallback diffCallback = new MessageDiffCallback(mesMessages, messages);
        final DiffUtil.DiffResult diffResult = DiffUtil.calculateDiff(diffCallback);

        // clear contacts and add
        messageAdapter.clear();
        messageAdapter.addAll(messages);

        diffResult.dispatchUpdatesTo((ListUpdateCallback) this); // calls adapter's notify methods after diff is computed
    }


    public void backgroundUpdate(){
        // Create the Handler object (on the main thread by default)
        h = new Handler();
// Define the code block to be executed
         Runnable runnableCode = new Runnable() {
            @Override
            public void run() {
                Backendless.Persistence.of(Message.class).find(c_query, new AsyncCallback<List<Message>>() {
                    @Override
                    public void handleResponse(List<Message> response) {
                        Log.d("DEBUG", response.toString());
                        if(response.size() != mainActivity.forum.size()){

                            for(int i = mesMessages.size(); i < response.size(); i++ ){
                                //mesMessages.add(response.get(i));
                                mainActivity.forum.add(response.get(i));
                                pushNotification(response.get(i).getSender(), response.get(i).getBody());
                            }

                        }

                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                messageAdapter.notifyDataSetChanged();
                                swipeForum.setRefreshing(false);
                            }
                        });
                    }

                    @Override
                    public void handleFault(BackendlessFault fault) {

                    }
                });

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        messageAdapter.notifyDataSetChanged();
                        swipeForum.setRefreshing(false);
                        listSize = messageAdapter.getCount();
                    }
                });

                h.postDelayed(this, 2000);
            }
        };
// Start the initial runnable task by posting through the handler
        h.post(runnableCode);
    }



    public void notification(){

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O){
            NotificationCompat.Builder mBuilder =
                    // Builder class for devices targeting API 26+ requires a channel ID
                    new NotificationCompat.Builder(getContext(), "myChannelId")
                            .setSmallIcon(R.mipmap.ic_launcher_round)
                            .setContentTitle("My notification")
                            .setContentText("Nouveau message !");

            NotificationManager mNotificationManager =
                    (NotificationManager) mainActivity.getSystemService(Context.NOTIFICATION_SERVICE);
// mId allows you to update the notification later on.
            mNotificationManager.notify(82, mBuilder.build());
        }else{
            NotificationCompat.Builder mBuilder =
                    // this Builder class is deprecated
                    new NotificationCompat.Builder(getContext(), "ID1")
                            .setSmallIcon(R.mipmap.ic_launcher_round)
                            .setContentTitle("ESIH")
                            .setContentText("Nouveau message !");

            NotificationManager mNotificationManager =
                    (NotificationManager) mainActivity.getSystemService(Context.NOTIFICATION_SERVICE);
// mId allows you to update the notification later on.
            mNotificationManager.notify(82, mBuilder.build());
        }

    }

    private boolean onSendMessage(int keyCode)
    {
        if( keyCode == KeyEvent.KEYCODE_ENTER)
        {
            String message = etMessageSend.getText().toString();

            if( message == null || message.equals( "" ) )
                return true;

            Backendless.Messaging.publish(Message.class, publishOptions, new AsyncCallback<MessageStatus>() {
                @Override
                public void handleResponse(MessageStatus response) {

                    PublishStatusEnum messageStatus = response.getStatus();
                    if( messageStatus == PublishStatusEnum.SCHEDULED )
                    {
                        etMessageSend.setText( "" );
                    }
                    else
                    {
                        Toast.makeText( getContext(), "Message status: " + messageStatus.toString(), Toast.LENGTH_SHORT );
                    }
                }

                @Override
                public void handleFault(BackendlessFault fault) {

                }
            });


            return true;
        }
        return false;
    }




    //Check if connection is available
    public Boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnectedOrConnecting();
    }


    //Check if connection is good
    public boolean isOnline() {
        Runtime runtime = Runtime.getRuntime();
        try {
            Process ipProcess = runtime.exec("/system/bin/ping -c 1 8.8.8.8");
            int exitValue = ipProcess.waitFor();
            return (exitValue == 0);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return false;
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
    }

}
