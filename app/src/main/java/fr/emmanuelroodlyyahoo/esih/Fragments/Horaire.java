package fr.emmanuelroodlyyahoo.esih.Fragments;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.backendless.Backendless;
import com.backendless.BackendlessUser;

import fr.emmanuelroodlyyahoo.esih.Activities.MainActivity;
import fr.emmanuelroodlyyahoo.esih.Activities.MainLivre;
import fr.emmanuelroodlyyahoo.esih.Activities.ReaderActivity;
import fr.emmanuelroodlyyahoo.esih.R;

import static android.Manifest.permission.READ_EXTERNAL_STORAGE;
import static android.Manifest.permission.WRITE_EXTERNAL_STORAGE;

/**
 * Created by Emmanuel Roodly on 18/09/2017.
 */

public class Horaire extends Fragment {

    BackendlessUser user;
    View racine;
    Intent i;
    MainActivity mainActivity;
    public Button btnHoraire1;
    public Button btnHorraire2;
    public Button btnReprise1;
    public Button btnReprise2;
    public Button btnExamen1;
    public Button btnExamen2;
    String final_link;
    String base_link;
    private static final int REQUEST_READ_STORAGE = 0;
    private static final int REQUEST_CAMERA = 0;
    private static final int REQUEST_WRITE_STORAGE = 0;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        racine = inflater.inflate(R.layout.horaire_ui, container, false);
        user = Backendless.UserService.CurrentUser();
        initViews();
        base_link = "https://api.backendless.com/A1CBE513-5446-4312-FF79-C93DA4C45000/44B0B647-AC2E-E528-FF43-699124CC5A00/files/horaire/";

        i = new Intent(getActivity(), ReaderActivity.class);



        btnHoraire1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                requestPermission();
                final_link = base_link + "Horaire-" + user.getProperty("niveau")+"-"+ user.getProperty("vacation")+"-Session1.pdf";
                i.putExtra("abook", final_link);
                i.putExtra("caption", "Horaire de cours - session 1");
                startActivity(i);
                mainActivity.overridePendingTransition(R.anim.right, R.anim.left);
            }
        });

        btnHorraire2.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                requestPermission();
                final_link = base_link + "Horaire-" + user.getProperty("niveau")+ "-"+ user.getProperty("vacation")+ "-Session2.pdf";
                i.putExtra("abook", final_link);
                i.putExtra("caption", "Horaire de cours - session 2");
                startActivity(i);
                mainActivity.overridePendingTransition(R.anim.right, R.anim.left);
            }
        });

        btnReprise1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                requestPermission();
                final_link = base_link + "Reprise-" + user.getProperty("niveau")+ "-"+ user.getProperty("vacation")+"-Session1.pdf";
                i.putExtra("abook", final_link);
                i.putExtra("caption", "Horaire d'examen de reprise - session 1");
                startActivity(i);
                mainActivity.overridePendingTransition(R.anim.right, R.anim.left);
            }
        });

        btnReprise2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                requestPermission();
                final_link = base_link + "Reprise-" + user.getProperty("niveau")+ "-"+ user.getProperty("vacation")+ "-Session2.pdf";
                i.putExtra("abook", final_link);
                i.putExtra("caption", "Horaire d'examen de reprise - session 1");
                startActivity(i);
                mainActivity.overridePendingTransition(R.anim.right, R.anim.left);
            }
        });

        btnExamen1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                requestPermission();
                final_link = base_link + "Examen-" + user.getProperty("niveau")+ "-"+ user.getProperty("vacation")+ "-Session1.pdf";
                i.putExtra("abook", final_link);
                i.putExtra("caption", "Horaire d'examen - session 1");
                startActivity(i);
                mainActivity.overridePendingTransition(R.anim.right, R.anim.left);
            }
        });

        btnExamen2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                requestPermission();
                final_link = base_link + "Examen-" + user.getProperty("niveau")+ "-"+ user.getProperty("vacation")+ "-Session2.pdf";
                i.putExtra("abook", final_link);
                i.putExtra("caption", "Horaire d'examen - session 1");
                startActivity(i);
                mainActivity.overridePendingTransition(R.anim.right, R.anim.left);
            }
        });

        return racine;
    }



    public void initViews(){
        btnHoraire1 = (Button) racine.findViewById(R.id.btnHoraire1);
        btnHorraire2 = (Button) racine.findViewById(R.id.btnHoraire2);
        btnReprise1 = (Button) racine.findViewById(R.id.btnReprise1);
        btnReprise2 = (Button) racine.findViewById(R.id.btnReprise2);
        btnExamen1 = (Button) racine.findViewById(R.id.btnExamen1);
        btnExamen2 = (Button) racine.findViewById(R.id.btnExamen2);
        mainActivity = (MainActivity) getActivity();
        mainActivity.fab.setVisibility(View.GONE);
    }

    public void requestPermission(){
        if(!requestWriteStoragePermission() && !requestExternalStoragePermission()){
            return;
        }

    }

    public boolean requestExternalStoragePermission() {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
            return true;
        }
        if (getActivity().checkSelfPermission(READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
            return true;
        }
        if (shouldShowRequestPermissionRationale(READ_EXTERNAL_STORAGE)) {
            Snackbar.make(btnHoraire1, R.string.permission_rationale, Snackbar.LENGTH_INDEFINITE)
                    .setAction(android.R.string.ok, new View.OnClickListener() {
                        @Override
                        @TargetApi(Build.VERSION_CODES.M)
                        public void onClick(View v) {
                            requestPermissions(new String[]{READ_EXTERNAL_STORAGE}, REQUEST_READ_STORAGE);
                        }
                    });
        } else {
            requestPermissions(new String[]{READ_EXTERNAL_STORAGE}, REQUEST_READ_STORAGE);
        }
        return false;
    }

    public boolean requestWriteStoragePermission() {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
            return true;
        }
        if (getActivity().checkSelfPermission(WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
            return true;
        }
        if (shouldShowRequestPermissionRationale(WRITE_EXTERNAL_STORAGE)) {
            Snackbar.make(btnHoraire1, R.string.permission_rationale, Snackbar.LENGTH_INDEFINITE)
                    .setAction(android.R.string.ok, new View.OnClickListener() {
                        @Override
                        @TargetApi(Build.VERSION_CODES.M)
                        public void onClick(View v) {
                            requestPermissions(new String[]{WRITE_EXTERNAL_STORAGE}, REQUEST_WRITE_STORAGE);
                        }
                    });
        } else {
            requestPermissions(new String[]{WRITE_EXTERNAL_STORAGE}, REQUEST_WRITE_STORAGE);
        }
        return false;
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
    }

}
