package fr.emmanuelroodlyyahoo.esih.Fragments;

import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.util.Log;
import android.view.GestureDetector;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ViewFlipper;

import com.backendless.Backendless;
import com.backendless.BackendlessUser;
import com.backendless.async.callback.AsyncCallback;
import com.backendless.exceptions.BackendlessFault;
import com.backendless.persistence.DataQueryBuilder;
import com.bumptech.glide.Glide;
import com.google.android.gms.vision.text.Text;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import fr.emmanuelroodlyyahoo.esih.Activities.DetailActivity;
import fr.emmanuelroodlyyahoo.esih.Activities.MainActivity;
import fr.emmanuelroodlyyahoo.esih.Adapters.InfoAdapter;
import fr.emmanuelroodlyyahoo.esih.Models.Infos;
import fr.emmanuelroodlyyahoo.esih.R;

import static android.icu.lang.UCharacter.GraphemeClusterBreak.V;

/**
 * Created by Emmanuel Roodly on 18/09/2017.
 */


public class Info extends Fragment {

    ArrayList<Infos> lesInfos;
    InfoAdapter infoAdapter;
    ListView lvInfo;
    Infos anInfo;
    SwipeRefreshLayout swipeContainer;
    private static final int SWIPE_MIN_DISTANCE = 120;
    private static final int SWIPE_THRESHOLD_VELOCITY = 200;
    private ViewFlipper mViewFlipper;
    private GestureDetector detector;
    public Animation.AnimationListener mAnimationListener;
    BackendlessUser user;
    View racine;
    final int PAGESIZE = 50;
    DataQueryBuilder c_query;

    ImageView iview1;
    ImageView iview2;
    ImageView iview3;
    ImageView iview4;
    ImageView iview5;
    ImageView iview6;
    ImageView iview7;
    ImageView iview8;
    TextView detail1;
    TextView detail2;
    TextView detail3;
    TextView detail4;
    TextView detail5;
    TextView detail6;
    TextView detail7;
    TextView detail8;

    MainActivity mainActivity;
    public ProgressBar pbInfo;
    TextView message;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        racine = inflater.inflate(R.layout.info_ui, container, false);
        lesInfos = new ArrayList<>();
        user = Backendless.UserService.CurrentUser();
        infoAdapter = new InfoAdapter(getContext(), lesInfos);
        lvInfo = (ListView) racine.findViewById(R.id.lvInfo);
        mainActivity = (MainActivity) getActivity();
        mViewFlipper = (ViewFlipper) racine.findViewById(R.id.view_flipper);
        initializeViews();
        mainActivity.fab.setVisibility(View.GONE);
        c_query = DataQueryBuilder.create();
        c_query.setPageSize(PAGESIZE);
        c_query.setSortBy("created DESC");
        message = (TextView) racine.findViewById(R.id.message);
        pbInfo = (ProgressBar) racine.findViewById(R.id.pbinfo);

        message.setVisibility(View.GONE);
        hideViews();

        swipeContainer = (SwipeRefreshLayout) racine.findViewById(R.id.swipeContainer);
        lvInfo.setAdapter(infoAdapter);

        detector = new GestureDetector(getContext(), new SwipeGestureDetector());

        mViewFlipper.setAutoStart(true);
        mViewFlipper.setFlipInterval(4000);
        mViewFlipper.startFlipping();
        mViewFlipper.setInAnimation(AnimationUtils.loadAnimation(getContext(), R.anim.left_in));
        mViewFlipper.setOutAnimation(AnimationUtils.loadAnimation(getContext(), R.anim.left_out));


        mViewFlipper.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(final View view, final MotionEvent event) {
                detector.onTouchEvent(event);
                return true;
            }
        });


        anInfo = new Infos();

        swipeContainer.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                message.setVisibility(View.GONE);
                infoAdapter.clear();
                loadTiles();
                populate();
            }
        });


        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                    if(mainActivity.isOnline()) {
                        loadTiles();
                        populate();
                        mainActivity.fab.setVisibility(View.VISIBLE);
                    }else{
                        pbInfo.setVisibility(View.GONE);
                        message.setVisibility(View.VISIBLE);
                        mainActivity.fab.setVisibility(View.GONE);
                        Snackbar.make(getView(), "Pas d'accès à Internet", Snackbar.LENGTH_LONG).setAction("Action", null).show();
                    }
                }
        }, 400);

        message.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                message.setVisibility(View.INVISIBLE);
                swipeContainer.setRefreshing(true);
                populate();
            }
        });


        lvInfo.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
                Intent i = new Intent(getContext(), DetailActivity.class);
                i.putExtra("info", lesInfos.get(position));
                startActivity(i);
            }
        });


        lvInfo.setOnScrollListener(new AbsListView.OnScrollListener() {
            private int mLastFirstVisibleItem;

            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {

            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {


                if(mLastFirstVisibleItem<firstVisibleItem)
                {
                    Log.i("SCROLLING DOWN","TRUE");
                    mainActivity.exitReveal();
                }
                if(mLastFirstVisibleItem>firstVisibleItem)
                {
                    Log.i("SCROLLING UP","TRUE");
                    mainActivity.enterReveal();
                }

                mLastFirstVisibleItem=firstVisibleItem;

            }
        });



        return racine;
    }


    private void populate() {
        Backendless.Persistence.of(Infos.class).find(c_query, new AsyncCallback<List<Infos>>() {
            @Override
            public void handleResponse(List<Infos> response) {
                Log.d("DEBUG", response.toString());
                lesInfos.addAll(response);
                infoAdapter.notifyDataSetChanged();
                showViews();
                pbInfo.setVisibility(View.GONE);
                swipeContainer.setRefreshing(false);
            }

            @Override
            public void handleFault(BackendlessFault fault) {
                Log.d("DEBUG", fault.getMessage());
                message.setVisibility(View.VISIBLE);
                pbInfo.setVisibility(View.GONE);
                Toast.makeText(mainActivity, "Erreur de connection", Toast.LENGTH_SHORT).show();
                swipeContainer.setRefreshing(false);
            }
        });
        infoAdapter.notifyDataSetChanged();

    }


    class SwipeGestureDetector extends GestureDetector.SimpleOnGestureListener {
        @Override
        public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY) {
            try {
                // right to left swipe
                if (e1.getX() - e2.getX() > SWIPE_MIN_DISTANCE && Math.abs(velocityX) > SWIPE_THRESHOLD_VELOCITY) {
                    mViewFlipper.setInAnimation(AnimationUtils.loadAnimation(getContext(), R.anim.left_in));
                    mViewFlipper.setOutAnimation(AnimationUtils.loadAnimation(getContext(), R.anim.left_out));
                    mViewFlipper.showNext();
                    return true;
                } else if (e2.getX() - e1.getX() > SWIPE_MIN_DISTANCE && Math.abs(velocityX) > SWIPE_THRESHOLD_VELOCITY) {
                    mViewFlipper.setInAnimation(AnimationUtils.loadAnimation(getContext(), R.anim.right_in));
                    mViewFlipper.setOutAnimation(AnimationUtils.loadAnimation(getContext(), R.anim.right_out));
                    mViewFlipper.showPrevious();
                    return true;
                }

            } catch (Exception e) {
                e.printStackTrace();
            }

            return false;
        }
    }


    public void hideViews(){
        mViewFlipper.setVisibility(View.GONE);
        lvInfo.setVisibility(View.GONE);
    }

    public void showViews(){
        mViewFlipper.setVisibility(View.VISIBLE);
        lvInfo.setVisibility(View.VISIBLE);
    }

    public void initializeViews(){
        iview1 = (ImageView) racine.findViewById(R.id.iview1);
        iview2 = (ImageView) racine.findViewById(R.id.iview2);
        iview3 = (ImageView) racine.findViewById(R.id.iview3);
        iview4 = (ImageView) racine.findViewById(R.id.iview4);
        iview5 = (ImageView) racine.findViewById(R.id.iview5);
        iview6 = (ImageView) racine.findViewById(R.id.iview6);
        iview7 = (ImageView) racine.findViewById(R.id.iview7);
        iview8 = (ImageView) racine.findViewById(R.id.iview8);

        detail1= (TextView) racine.findViewById(R.id.detail1);
        detail2= (TextView) racine.findViewById(R.id.detail2);
        detail3= (TextView) racine.findViewById(R.id.detail3);
        detail4= (TextView) racine.findViewById(R.id.detail4);
        detail5= (TextView) racine.findViewById(R.id.detail5);
        detail6= (TextView) racine.findViewById(R.id.detail6);
        detail7= (TextView) racine.findViewById(R.id.detail7);
        detail8= (TextView) racine.findViewById(R.id.detail8);
    }

    public void loadTiles(){

        Glide.with(getContext()).load("https://api.backendless.com/A1CBE513-5446-4312-FF79-C93DA4C45000/770EBAF5-E9ED-E035-FF22-55EDF892F500/files/img/test1.jpg").fitCenter().placeholder(R.drawable.logo2).into(iview1);
        Glide.with(getContext()).load("https://api.backendless.com/A1CBE513-5446-4312-FF79-C93DA4C45000/770EBAF5-E9ED-E035-FF22-55EDF892F500/files/img/test2.jpg").fitCenter().placeholder(R.drawable.logo2).into(iview2);
        Glide.with(getContext()).load("https://api.backendless.com/A1CBE513-5446-4312-FF79-C93DA4C45000/770EBAF5-E9ED-E035-FF22-55EDF892F500/files/img/test3.jpg").fitCenter().placeholder(R.drawable.logo2).into(iview3);
        Glide.with(getContext()).load("https://api.backendless.com/A1CBE513-5446-4312-FF79-C93DA4C45000/770EBAF5-E9ED-E035-FF22-55EDF892F500/files/img/test4.jpg").fitCenter().placeholder(R.drawable.logo2).into(iview4);
        Glide.with(getContext()).load("https://api.backendless.com/A1CBE513-5446-4312-FF79-C93DA4C45000/770EBAF5-E9ED-E035-FF22-55EDF892F500/files/img/test5.jpg").fitCenter().placeholder(R.drawable.logo2).into(iview5);
        Glide.with(getContext()).load("https://api.backendless.com/A1CBE513-5446-4312-FF79-C93DA4C45000/770EBAF5-E9ED-E035-FF22-55EDF892F500/files/img/test6.jpg").fitCenter().placeholder(R.drawable.logo2).into(iview6);
        Glide.with(getContext()).load("https://api.backendless.com/A1CBE513-5446-4312-FF79-C93DA4C45000/770EBAF5-E9ED-E035-FF22-55EDF892F500/files/img/test9.jpg").fitCenter().placeholder(R.drawable.logo2).into(iview7);
        Glide.with(getContext()).load("https://api.backendless.com/A1CBE513-5446-4312-FF79-C93DA4C45000/770EBAF5-E9ED-E035-FF22-55EDF892F500/files/img/test8.jpg").fitCenter().placeholder(R.drawable.logo2).into(iview8);

        detail7.setText("La biblio de l'ESIH");
        detail1.setText("Innovation sans limite");
        detail6.setText("Nos impressions 3D");
        detail2.setText(" ");
        detail3.setText(" ");
        detail4.setText("Promotion 2008-2009 de l'ESIH");
        detail5.setText("Devise de L'ESIH");
        detail8.setText("Nos ingénieurs");

    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
    }




}


