package fr.emmanuelroodlyyahoo.esih.Fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.astuetz.PagerSlidingTabStrip;
import com.backendless.Backendless;
import com.backendless.BackendlessUser;

import fr.emmanuelroodlyyahoo.esih.Activities.MainActivity;
import fr.emmanuelroodlyyahoo.esih.Adapters.SampleFragmentPagerAdapter;
import fr.emmanuelroodlyyahoo.esih.R;

import static fr.emmanuelroodlyyahoo.esih.R.id.parent;

/**
 * Created by Emmanuel Roodly on 18/09/2017.
 */

public class Notes extends Fragment {

    FragmentManager fm;
    BackendlessUser user;
    MainActivity mainActivity;
    NotespagerAdapter adapter;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.notes_ui, null, false);
        Toolbar toolbar = (Toolbar) v.findViewById(R.id.toolbar);
        user = Backendless.UserService.CurrentUser();


        ViewPager viewPager = (ViewPager) v.findViewById(R.id.viewpager);
        viewPager.setAdapter(new NotespagerAdapter(getFragmentManager()));
        mainActivity = (MainActivity) getActivity();
        mainActivity.fab.setVisibility(View.GONE);


        PagerSlidingTabStrip tabsStrip = (PagerSlidingTabStrip) v.findViewById(R.id.tabs);
        // Attach the view pager to the tab strip
        tabsStrip.setViewPager(viewPager);


        return v;
    }



    public  class NotespagerAdapter extends FragmentPagerAdapter{

        final int PAGE_COUNT = 2;
        private String tabTitle[] = {"Session 1", "Session 2"};

        public NotespagerAdapter(FragmentManager fm){
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            if(position == 0){
                return Session1.newInstance(0);
            }else if (position == 1){
                return  Session2.newInstance(1);
            }else{
                return  null;
            }
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return tabTitle[position];
        }

        @Override
        public int getCount() {
            return PAGE_COUNT;
        }
    }


    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
    }

    @Override
    public void onStop() {
        super.onStop();
    }
}
