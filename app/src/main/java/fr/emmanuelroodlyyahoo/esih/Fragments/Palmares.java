package fr.emmanuelroodlyyahoo.esih.Fragments;

import android.support.v4.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.backendless.Backendless;
import com.backendless.BackendlessUser;

import fr.emmanuelroodlyyahoo.esih.Activities.*;
import fr.emmanuelroodlyyahoo.esih.R;

/**
 * Created by Emmanuel on 3/15/2018.
 */

public class Palmares extends Fragment{

    BackendlessUser user;
    View racine;
    Intent i;
    MainActivity mainActivity;
    public Button btnNoteL1;
    public Button btnNoteL2;
    public Button btnNoteL3;
    public Button btnNoteLicense;
    public Button btnNoteM1;
    public Button btnNoteM2;




    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        racine = inflater.inflate(R.layout.palmares_ui, container, false);
        user = Backendless.UserService.CurrentUser();
        initViews();

        switch (user.getProperty("niveau").toString()) {
            case "L1":
                btnNoteL2.setVisibility(View.GONE);
                btnNoteL3.setVisibility(View.GONE);
                btnNoteLicense.setVisibility(View.GONE);
                btnNoteM1.setVisibility(View.GONE);
                btnNoteM2.setVisibility(View.GONE);
                break;
            case "L2":
                btnNoteL3.setVisibility(View.GONE);
                btnNoteLicense.setVisibility(View.GONE);
                btnNoteM1.setVisibility(View.GONE);
                btnNoteM2.setVisibility(View.GONE);
                break;
            case "L3":
                btnNoteLicense.setVisibility(View.GONE);
                btnNoteM1.setVisibility(View.GONE);
                btnNoteM2.setVisibility(View.GONE);
                break;
            case "License":
                btnNoteM1.setVisibility(View.GONE);
                btnNoteM2.setVisibility(View.GONE);
                break;
            case "M1":
                btnNoteM2.setVisibility(View.GONE);
                break;
            case "M2":
                //Do nothing
                break;
            default:
                break;
        }


        i = new Intent(getActivity(), NotesActivity.class);



        btnNoteL1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                i.putExtra("NIVEAU", "L1");
                startActivity(i);
                mainActivity.overridePendingTransition(R.anim.right, R.anim.left);
            }
        });

        btnNoteL2.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                i.putExtra("NIVEAU", "L2");
                startActivity(i);
                mainActivity.overridePendingTransition(R.anim.right, R.anim.left);
            }
        });

        btnNoteL3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                i.putExtra("NIVEAU", "L3");
                startActivity(i);
                mainActivity.overridePendingTransition(R.anim.right, R.anim.left);
            }
        });

        btnNoteLicense.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                i.putExtra("NIVEAU", "License");
                startActivity(i);
                mainActivity.overridePendingTransition(R.anim.right, R.anim.left);
            }
        });

        btnNoteM1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                i.putExtra("NIVEAU", "M1");
                startActivity(i);
                mainActivity.overridePendingTransition(R.anim.right, R.anim.left);
            }
        });

        btnNoteM2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                i.putExtra("NIVEAU", "M2");
                startActivity(i);
                mainActivity.overridePendingTransition(R.anim.right, R.anim.left);
            }
        });

        return racine;
    }



    public void initViews(){
        btnNoteL1 = (Button) racine.findViewById(R.id.btnNoteL1);
        btnNoteL2 = (Button) racine.findViewById(R.id.btnNoteL2);
        btnNoteL3 = (Button) racine.findViewById(R.id.btnNoteL3);
        btnNoteLicense = (Button) racine.findViewById(R.id.btnNoteLicense);
        btnNoteM1 = (Button) racine.findViewById(R.id.btnNoteM1);
        btnNoteM2 = (Button) racine.findViewById(R.id.btnNoteM2);
        mainActivity = (MainActivity) getActivity();
        mainActivity.fab.setVisibility(View.GONE);
    }





}
