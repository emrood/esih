package fr.emmanuelroodlyyahoo.esih.Fragments;

import android.annotation.TargetApi;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.view.WindowManager;
import android.view.Display;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.widget.Toast;

import com.astuetz.PagerSlidingTabStrip;
import com.backendless.Backendless;
import com.backendless.BackendlessUser;
import com.backendless.async.callback.AsyncCallback;
import com.backendless.exceptions.BackendlessFault;
import com.backendless.files.BackendlessFile;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;

import java.io.File;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import fr.emmanuelroodlyyahoo.esih.Activities.MainActivity;
import fr.emmanuelroodlyyahoo.esih.Adapters.SampleFragmentPagerAdapter;
import fr.emmanuelroodlyyahoo.esih.R;
import fr.emmanuelroodlyyahoo.esih.utils.Defaults;
import jp.wasabeef.glide.transformations.BlurTransformation;
import jp.wasabeef.glide.transformations.RoundedCornersTransformation;

import static android.Manifest.permission.READ_EXTERNAL_STORAGE;
import static android.Manifest.permission.WRITE_EXTERNAL_STORAGE;

/**
 * Created by Emmanuel on 04-Dec-17.
 */

public class Profil extends Fragment {

    private static final String TAG = null ;
    public ImageView ivProfilAvatar;
    ImageView ivBanner;
    TextView tvNom;
    TextView tvEmail;
    TextView tvReprise1;
    TextView tvReprise2;
    TextView tvPhone;
    TextView tvBalance;
    TextView tvDomaine;
    TextView tvNiveau;
    TextView tvMemberDate;
    TextView tvSexe;
    TextView tvDateN;
    TextView tvAdresse;
    TextView tvNationaleId;
    TextView tvVacation;

    FragmentManager fm;
    BackendlessUser user;
    public View view;
    DateFormat df;
    public ProgressBar pbUserAvatar;
    Date dateCreated;
    MainActivity mainActivity;
    static final int REQUEST_IMAGE_CAPTURE = 50;
    static final int IMAGE_FROM_GALERY = 82;
    private Bitmap mImageBitmap;
    private String mCurrentPhotoPath;
    String u_avatar;
    byte[] avatar_Array =  null;
    String avatarPath = null;
    private static final int REQUEST_READ_STORAGE = 0;
    private static final int REQUEST_CAMERA = 0;
    private static final int REQUEST_WRITE_STORAGE = 0;



    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.profil_ui, null, false);
        user = Backendless.UserService.CurrentUser();
        df = new SimpleDateFormat("MM/dd/yyyy");
        mainActivity = (MainActivity) getActivity();
        mainActivity.fab.setVisibility(View.GONE);

        try{
            Bundle bundle = this.getArguments();
            avatar_Array = bundle.getByteArray("avatar");
            avatarPath = bundle.getString("avatarPath");

        }catch (Exception e){

        }

        try{

            dateCreated = df.parse(user.getProperty("created").toString());
        }catch (Exception e){
            e.printStackTrace();

        }


        InitViews();


        try{
            u_avatar = user.getProperty("avatar").toString();
            pbUserAvatar.setVisibility(View.VISIBLE);
        }catch(Exception e){
            e.printStackTrace();
            pbUserAvatar.setVisibility(View.GONE);
        }

        setDataInViews();


        ivProfilAvatar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                requestPermission();
                selectImage();
            }
        });
        return view;
    }

    public void InitViews() {
        ivProfilAvatar = (ImageView) view.findViewById(R.id.ivProfilAvatar);
        tvNom = (TextView) view.findViewById(R.id.tvNom);
        tvEmail = (TextView) view.findViewById(R.id.tvEmail);
        tvReprise1 = (TextView) view.findViewById(R.id.tvReprise1);
        tvReprise2 = (TextView) view.findViewById(R.id.tvReprise2);
        tvPhone = (TextView) view.findViewById(R.id.tvPhone);
        tvBalance = (TextView) view.findViewById(R.id.tvBalance);
        tvDomaine = (TextView) view.findViewById(R.id.tvDomaine);
        tvNiveau = (TextView) view.findViewById(R.id.tvNiveau);
        tvMemberDate = (TextView) view.findViewById(R.id.tvMemberDate);
        pbUserAvatar = (ProgressBar) view.findViewById(R.id.pbUserAvatar);
        ivBanner = (ImageView) view.findViewById(R.id.ivBanner);
        tvSexe = (TextView) view.findViewById(R.id.tvSexe);
        tvDateN = (TextView) view.findViewById(R.id.tvDateN);
        tvAdresse = (TextView) view.findViewById(R.id.tvAdresse);
        tvNationaleId = (TextView) view.findViewById(R.id.tvnationaleId);
        tvVacation = (TextView) view.findViewById(R.id.tvVacation);
    }


    public void setDataInViews() {
        tvNom.setText("Nom : " + user.getProperty("name").toString());
        tvEmail.setText("Email : " + user.getEmail());
        tvReprise1.setText("Reprise(s) - Session 1 : "+ user.getProperty("repriseS1").toString());
        tvReprise2.setText( "Reprise(s) - Session 2 : "+ user.getProperty("repriseS2").toString());
        tvPhone.setText("Telephone : " + user.getProperty("phone").toString());
        tvDomaine.setText("Domaine: " + user.getProperty("domaine").toString());
        tvNiveau.setText("Niveau: " + user.getProperty("niveau").toString());
        tvMemberDate.setText("Date d'inscription : " + String.valueOf(user.getProperty("created")));
        tvBalance.setText("Balance : " + String.valueOf(user.getProperty("balance"))+ "USD");
        tvDateN.setText("Date de naissance : " + user.getProperty("dateN").toString());
        tvSexe.setText("Sexe : " + user.getProperty("sexe").toString());
        tvVacation.setText("Vacation : " + user.getProperty("vacation").toString());
        tvNationaleId.setText("NIF ou CIN : " + Html.fromHtml(user.getProperty("nationaleId").toString()));
        tvAdresse.setText("Adresse : " + user.getProperty("adresse").toString());


        Glide.with(getContext()).load(R.drawable.bannertest).fitCenter().placeholder(R.drawable.logo2).into(ivBanner);

        if(!TextUtils.isEmpty(u_avatar) || u_avatar != null){
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    Glide.with(getContext())
                            .load(user.getProperty("avatar").toString())
                            .listener(new RequestListener<String, GlideDrawable>() {
                                @Override
                                public boolean onException(Exception e, String model, Target<GlideDrawable> target, boolean isFirstResource) {
                                    pbUserAvatar.setVisibility(View.GONE);
                                    return false; // important to return false so the error placeholder can be placed
                                }

                                @Override
                                public boolean onResourceReady(GlideDrawable resource, String model, Target<GlideDrawable> target, boolean isFromMemoryCache, boolean isFirstResource) {
                                    pbUserAvatar.setVisibility(View.GONE);
                                    return false;
                                }
                            }).bitmapTransform(new RoundedCornersTransformation(getContext(), 30, 10))
                            .placeholder(R.drawable.user).error(R.drawable.user).into(ivProfilAvatar);

                }
            }, 600);
        }

    }


    public void changePicture(){

        Intent i = new Intent(Intent.ACTION_PICK);
        i.setType("image/*");
        getActivity().startActivityForResult(Intent.createChooser(i, "Choisir une photo"), REQUEST_IMAGE_CAPTURE );
    }


    public static void refreshAvatar(){

    }

    public void uploadFile(Bitmap picture, String name){

        Backendless.Files.Android.upload(picture, Bitmap.CompressFormat.PNG, 100, name + ".png", Defaults.DEFAULT_PATH_AVATAR, new AsyncCallback<BackendlessFile>() {
            @Override
            public void handleResponse(BackendlessFile response) {
                Log.d("DEBUG", response.toString());
                Toast.makeText(mainActivity, "Photo upladed", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void handleFault(BackendlessFault fault) {
                Log.d("Debug", fault.getMessage());
                Toast.makeText(mainActivity, "Photo upladed", Toast.LENGTH_SHORT).show();
            }
        });
    }

    public void getImageResult(Bitmap img){
        ivProfilAvatar = (ImageView) view.findViewById(R.id.ivProfilAvatar);
        ivProfilAvatar.setImageBitmap(img);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_IMAGE_CAPTURE && resultCode == getActivity().RESULT_OK) {
            try {
                mImageBitmap = MediaStore.Images.Media.getBitmap(getActivity().getContentResolver(), Uri.parse(mCurrentPhotoPath));
                ivProfilAvatar.setImageBitmap(mImageBitmap);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    private void selectImage() {
        final CharSequence[] options = {"Choisir dans la galerie","Annulé" };
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setTitle("Photo de profil");
        builder.setItems(options, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {

                if (options[item].equals("Choisir dans la galerie"))
                {
                    changePicture();
                }
                else if (options[item].equals("Annulé")) {
                    dialog.dismiss();
                }
            }
        });
        builder.show();
    }

    public void requestPermission(){
        if(!requestWriteStoragePermission() && !requestExternalStoragePermission()){
            return;
        }

    }

    public boolean requestExternalStoragePermission() {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
            return true;
        }
        if (getActivity().checkSelfPermission(READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
            return true;
        }
        if (shouldShowRequestPermissionRationale(READ_EXTERNAL_STORAGE)) {
            Snackbar.make(ivProfilAvatar, R.string.permission_rationale, Snackbar.LENGTH_INDEFINITE)
                    .setAction(android.R.string.ok, new View.OnClickListener() {
                        @Override
                        @TargetApi(Build.VERSION_CODES.M)
                        public void onClick(View v) {
                            requestPermissions(new String[]{READ_EXTERNAL_STORAGE}, REQUEST_READ_STORAGE);
                        }
                    });
        } else {
            requestPermissions(new String[]{READ_EXTERNAL_STORAGE}, REQUEST_READ_STORAGE);
        }
        return false;
    }

    public boolean requestWriteStoragePermission() {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
            return true;
        }
        if (getActivity().checkSelfPermission(WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
            return true;
        }
        if (shouldShowRequestPermissionRationale(WRITE_EXTERNAL_STORAGE)) {
            Snackbar.make(ivProfilAvatar, R.string.permission_rationale, Snackbar.LENGTH_INDEFINITE)
                    .setAction(android.R.string.ok, new View.OnClickListener() {
                        @Override
                        @TargetApi(Build.VERSION_CODES.M)
                        public void onClick(View v) {
                            requestPermissions(new String[]{WRITE_EXTERNAL_STORAGE}, REQUEST_WRITE_STORAGE);
                        }
                    });
        } else {
            requestPermissions(new String[]{WRITE_EXTERNAL_STORAGE}, REQUEST_WRITE_STORAGE);
        }
        return false;
    }


    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onPause() {

        super.onPause();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
    }


}
