package fr.emmanuelroodlyyahoo.esih.Fragments;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.util.Log;
import android.view.GestureDetector;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.ViewFlipper;

import com.backendless.Backendless;
import com.backendless.BackendlessUser;
import java.util.ArrayList;
import fr.emmanuelroodlyyahoo.esih.Activities.DetailActivity;
import fr.emmanuelroodlyyahoo.esih.Activities.MainActivity;
import fr.emmanuelroodlyyahoo.esih.Activities.MainLivre;
import fr.emmanuelroodlyyahoo.esih.R;
import fr.emmanuelroodlyyahoo.esih.utils.MonConnecteur;

/**
 * Created by Emmanuel on 24-Nov-17.
 */

public class Ressources extends Fragment {

    BackendlessUser user;
    View racine;
    ImageButton btAllBooks;
    ImageButton btProgrammation;
    ImageButton btReseau;
    ImageButton btGestion;
    Intent i;
    private String choice;
    Activity activity;
    MainActivity mainActivity;

    public MonConnecteur monConnecteur;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        racine = inflater.inflate(R.layout.ressources_ui, container, false);
        user = Backendless.UserService.CurrentUser();
        initViews();
        i = new Intent(getActivity(), MainLivre.class);

        btAllBooks.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                i.putExtra("choix", "All");
                startActivity(i);
            }
        });

        btProgrammation.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                i.putExtra("choix", "Programmation");
                startActivity(i);
            }
        });

        btReseau.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                i.putExtra("choix", "Reseau");
                startActivity(i);
            }
        });

        btGestion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                i.putExtra("choix", "img_gestion");
                startActivity(i);
            }
        });

        return racine;
    }


    public void initViews(){
        btAllBooks = racine.findViewById(R.id.btAllBooks);
        btProgrammation = racine.findViewById(R.id.btProgramation);
        btGestion = racine.findViewById(R.id.btGestion);
        btReseau = racine.findViewById(R.id.btReseau);
        mainActivity = (MainActivity) getActivity();
        mainActivity.fab.setVisibility(View.GONE);
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
    }





}

