package fr.emmanuelroodlyyahoo.esih.Fragments;

import android.app.ProgressDialog;
import android.graphics.Bitmap;
import android.support.v4.app.DialogFragment;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.backendless.Backendless;
import com.backendless.BackendlessUser;
import com.backendless.async.callback.AsyncCallback;
import com.backendless.exceptions.BackendlessFault;
import com.backendless.files.BackendlessFile;
import com.backendless.files.router.OutputStreamRouter;
import com.github.barteksc.pdfviewer.util.FileUtils;

import org.apache.commons.io.FilenameUtils;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.*;

import fr.emmanuelroodlyyahoo.esih.Activities.MainActivity;
import fr.emmanuelroodlyyahoo.esih.R;
import fr.emmanuelroodlyyahoo.esih.utils.Defaults;
import fr.emmanuelroodlyyahoo.esih.utils.IOUtil;

/**
 * Created by Emmanuel on 12-Dec-17.
 */

public class SendFile extends DialogFragment {

    public TextView tvPicker;
    Button btnSendFile;
    ImageButton btnCancel;
    ProgressBar pbSendFile;
    public String fileName;
    BackendlessUser user;
    public String Fpath = null;
    public File fileChoosed = null;
    public String remotePath = "/devoir/received";
    private static final int CHOOSE_FILE_REQUESTCODE = 30;
    public byte[] fileBytes = "Zajdnsiniscninc \n jnsjbsucbibd".getBytes();
    FileInputStream fileInputStream;
    OutputStreamRouter fileStream;
    FileOutputStream fileOutputStream;
    ProgressDialog progressDialog;
    public Bitmap photo = null;
    public String extension = null;
    MainActivity mainActivity;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.sendfile_fragment, null);
        tvPicker = (TextView) root.findViewById(R.id.tvPicker);
        btnSendFile = (Button) root.findViewById(R.id.btnSendFile);
        btnCancel = (ImageButton) root.findViewById(R.id.ibCancel2);
        user = Backendless.UserService.CurrentUser();
        Backendless.setUrl(Defaults.SERVER_URL);
        Backendless.initApp(getActivity(), Defaults.APPLICATION_ID, Defaults.API_KEY);
        pbSendFile = (ProgressBar) root.findViewById(R.id.pbSendFile);
        tvPicker.setClickable(true);
        tvPicker.setEnabled(true);
        btnSendFile.setClickable(false);
        btnSendFile.setEnabled(false);
        pbSendFile.setVisibility(View.INVISIBLE);
        mainActivity = (MainActivity) getActivity();

        tvPicker.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
                intent.setType("file/*");
                getActivity().startActivityForResult(intent, CHOOSE_FILE_REQUESTCODE);
            }
        });


        btnSendFile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(TextUtils.equals(extension, "jpg") || TextUtils.equals(extension, "png")){
                    if(photo != null){
                        uploadFile(photo, fileName);
                    }
                }else if(TextUtils.equals(extension, "pdf")){
                    uploadPdf(photo);
                }else{
                    Toast.makeText(getContext(), "Format non prise en charge", Toast.LENGTH_SHORT).show();
                }

            }
        });



        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });

        return root;
    }


    public void uploadFile(Bitmap picture, String name) {
        progressDialog = ProgressDialog.show(getActivity(), "", "Envoie du fichier", true);
        Backendless.Files.Android.upload(picture, Bitmap.CompressFormat.PNG, 100, name + ".png", Defaults.DEFAULT_PATH_ROOT, new AsyncCallback<BackendlessFile>() {
            @Override
            public void handleResponse(BackendlessFile response) {
                Log.d("DEBUG", response.toString());
                Toast.makeText(getContext(), "Image envoye", Toast.LENGTH_SHORT).show();
                mainActivity.confirmationMail(fileName);
                progressDialog.dismiss();
                dismiss();
            }

            @Override
            public void handleFault(BackendlessFault fault) {
                Log.d("Debug", fault.getMessage());
                Toast.makeText(getContext(), "Erreur lors de l'envoie", Toast.LENGTH_SHORT).show();
                progressDialog.dismiss();
            }
        });



    }







    public void uploadPdf(Bitmap document){

        progressDialog = ProgressDialog.show(getActivity(), "", "Envoie du fichier", true);
        /*
        Backendless.Files.upload(fileChoosed, Defaults.DEFAULT_PATH_ROOT, new AsyncCallback<BackendlessFile>() {
            @Override
            public void handleResponse(BackendlessFile response) {
                Log.d("DEBUG", response.toString());
                Toast.makeText(getContext(), "File Uploaded", Toast.LENGTH_SHORT).show();
                progressDialog.dismiss();
                dismiss();
            }

            @Override
            public void handleFault(BackendlessFault fault) {
                Log.d("DEBUG", fault.getMessage());
                Toast.makeText(getContext(), "file upload error", Toast.LENGTH_SHORT).show();
                progressDialog.dismiss();
            }
        });*/

        String name = FilenameUtils.getBaseName(fileName);



        Backendless.Files.saveFile(Defaults.DEFAULT_PATH_ROOT, name, convertFileToByteArray(fileChoosed), new AsyncCallback<String>() {
            @Override
            public void handleResponse(String response) {
                Log.d("DEBUG", response.toString());
                Toast.makeText(getContext(), "Fichier envoye", Toast.LENGTH_SHORT).show();
                mainActivity.confirmationMail(fileName);
                progressDialog.dismiss();
                dismiss();
            }

            @Override
            public void handleFault(BackendlessFault fault) {
                Log.d("DEBUG", fault.getMessage());
                Toast.makeText(getContext(), "Erreur lors de l'envoie", Toast.LENGTH_SHORT).show();
                progressDialog.dismiss();
            }
        });

    }

    public void checkFilePicked() {
        if (!TextUtils.equals(tvPicker.getText(), getString(R.string.pick_file)) && !TextUtils.equals(tvPicker.getText(), " ")) {
            btnSendFile.setClickable(true);
            btnSendFile.setEnabled(true);
        }
    }

    public static byte[] convertFileToByteArray(File f) {
        byte[] byteArray = null;
        try {
            InputStream inputStream = new FileInputStream(f);
            ByteArrayOutputStream bos = new ByteArrayOutputStream();
            byte[] b = new byte[1024 * 8];
            int bytesRead = 0;

            while ((bytesRead = inputStream.read(b)) != -1) {
                bos.write(b, 0, bytesRead);
            }

            byteArray = bos.toByteArray();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return byteArray;
    }


    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
    }


}


