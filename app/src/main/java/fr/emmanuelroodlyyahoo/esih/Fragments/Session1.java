package fr.emmanuelroodlyyahoo.esih.Fragments;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.backendless.Backendless;
import com.backendless.BackendlessUser;
import com.backendless.async.callback.AsyncCallback;
import com.backendless.exceptions.BackendlessFault;
import com.backendless.persistence.DataQueryBuilder;

import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.List;

import fr.emmanuelroodlyyahoo.esih.Activities.*;
import fr.emmanuelroodlyyahoo.esih.Adapters.NotesAdapter;
import fr.emmanuelroodlyyahoo.esih.Models.Note;
import fr.emmanuelroodlyyahoo.esih.R;

/**
 * Created by Emmanuel Roodly on 18/09/2017.
 */

public class Session1 extends Fragment {

    BackendlessUser user;

    public static final String ARG_PAGE = "ARG_PAGE";
    public static final String ARG_NOTE = "Note";
    TextView adminDirection;
    private int mPage;
    ListView lvNotes;
    ArrayList<Note> mesNotes;
    NotesAdapter notesAdapter;
    SwipeRefreshLayout swipeNote;
    DataQueryBuilder c_query;
    View view;
    NotesActivity notesActivity;
    String niveau;

    public static Session1 newInstance(int page) {
        Bundle args = new Bundle();
        args.putInt(ARG_PAGE, page);
        Session1 fragment = new Session1();
        fragment.setArguments(args);
        return fragment;
    }

    public static Session1 newScreen(int page, String niveau) {
        Bundle args = new Bundle();
        args.putInt(ARG_PAGE, page);
        args.putString("NIVEAU", niveau);
        Session1 fragment = new Session1();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mPage = getArguments().getInt(ARG_PAGE);
        niveau = getArguments().getString("NIVEAU");
        mesNotes = new ArrayList<>();

        notesActivity = (NotesActivity) getActivity();
        //notesAdapter = new NotesAdapter(getContext(), mainActivity.session1);
        notesAdapter = new NotesAdapter(getContext(), mesNotes);
        user = Backendless.UserService.CurrentUser();
    }

    // Inflate the fragment layout we defined above for this fragment
    // Set the associated text for the title
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.session_ui, container, false);
        TextView tvTitle = (TextView) view.findViewById(R.id.tvPage);
        lvNotes = (ListView) view.findViewById(R.id.lvNotes);
        mesNotes = new ArrayList<>();

        adminDirection = (TextView) view.findViewById(R.id.adminDirection);
        adminDirection.setVisibility(View.GONE);
        notesAdapter = new NotesAdapter(getContext(), mesNotes);
        user = Backendless.UserService.CurrentUser();
        lvNotes.setAdapter(notesAdapter);

        swipeNote = (SwipeRefreshLayout) view.findViewById(R.id.swipenote);

        /*
        if(mainActivity.session1.isEmpty()){
            notesAdapter.clear();
            swipeNote.setRefreshing(true);
            notesAdapter.notifyDataSetChanged();
            swipeNote.setRefreshing(false);
            Toast.makeText(mainActivity, "Non disponible", Toast.LENGTH_SHORT).show();
        } */
        swipeNote.setRefreshing(true);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                    populateNotes();
                    notesAdapter.notifyDataSetChanged();
                    swipeNote.setRefreshing(false);
            }
        }, 600);


        swipeNote.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                adminDirection.setVisibility(View.INVISIBLE);
                notesAdapter.clear();
                populateNotes();
                swipeNote.setRefreshing(false);
            }
        });

        tvTitle.setText("Fragment #" + mPage);
        return view;
    }

    public void populateNotes() {
        // mesNotes.addAll(Note.fromMap(user.getProperties("")));

        String query = "ID = " + "'" + user.getUserId() + "' and session = 1 and niveau = '" + niveau + "'";
        c_query = DataQueryBuilder.create();
        c_query.setWhereClause(query);
        c_query.setPageSize(20);

        Backendless.Persistence.of(Note.class).find(c_query, new AsyncCallback<List<Note>>() {
            @Override
            public void handleResponse(List<Note> response) {
                Log.d("DEBUG", response.toString());
                //notesAdapter.addAll(response);
                mesNotes.addAll(response);
                notesAdapter.notifyDataSetChanged();
                swipeNote.setRefreshing(false);
                if(response.isEmpty()){
                    adminDirection.setText(R.string.notes_unavailable);
                    //Toast.makeText(notesActivity, "Non disponible", Toast.LENGTH_SHORT).show();
                    adminDirection.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void handleFault(BackendlessFault fault) {
                Log.d("DEBUG", fault.toString());
                adminDirection.setText(R.string.error_server);
                swipeNote.setRefreshing(false);
                adminDirection.setVisibility(View.VISIBLE);
            }
        });

        notesAdapter.notifyDataSetChanged();
    }


    public Boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnectedOrConnecting();
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onPause() {

        super.onPause();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
    }


}
