package fr.emmanuelroodlyyahoo.esih.Fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.backendless.Backendless;
import com.backendless.BackendlessUser;

import fr.emmanuelroodlyyahoo.esih.Activities.ReaderActivity;
import fr.emmanuelroodlyyahoo.esih.R;

/**
 * Created by Emmanuel on 2/8/2018.
 */

public class Settings extends Fragment {

    View racine;
    BackendlessUser user;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        racine = inflater.inflate(R.layout.settings_ui, container, false);
        user = Backendless.UserService.CurrentUser();
        initViews();


        return racine;
    }

    public void initViews(){
        //To initialize views
    }

}
