package fr.emmanuelroodlyyahoo.esih.Models;

import java.io.Serializable;

/**
 * Created by Emmanuel on 12-Dec-17.
 */

public class AdminMessage implements Serializable {

    private String title;
    private String detail;
    private String niveau;
    private String vacation;
    private String link;
    private String created;
    private String objectId;
    private String img;
    private String video;
    private String dateRemise;
    private String file_name;

    public String getFile_name() {
        return file_name;
    }

    public void setFile_name(String file_name) {
        this.file_name = file_name;
    }



    public String getDateRemise() {
        return dateRemise;
    }

    public void setDateRemise(String dateRemise) {
        this.dateRemise = dateRemise;
    }



    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }

    public String getVideo() {
        return video;
    }

    public void setVideo(String video) {
        this.video = video;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDetail() {
        return detail;
    }

    public void setDetail(String detail) {
        this.detail = detail;
    }

    public String getNiveau() {
        return niveau;
    }

    public void setNiveau(String niveau) {
        this.niveau = niveau;
    }

    public String getVacation() {
        return vacation;
    }

    public void setVacation(String vacation) {
        this.vacation = vacation;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public String getCreated() {
        return created;
    }

    public void setCreated(String created) {
        this.created = created;
    }

    public String getObjectId() {
        return objectId;
    }

    public void setObjectId(String objectId) {
        this.objectId = objectId;
    }

    public AdminMessage() {

    }

    public AdminMessage(String title, String detail, String niveau, String vacation) {
        this.title = title;
        this.detail = detail;
        this.niveau = niveau;
        this.vacation = vacation;
    }


}
