package fr.emmanuelroodlyyahoo.esih.Models;

/**
 * Created by Emmanuel on 10/2/2017.
 */

public class Horaire {

    //Atributs

    private String jour;
    private String heure;
    private String matiere;
    private String professeur;
    private String niveau;
    private int position;
    private  String telephone;


    //Getters and Setters

    public String getJour() {
        return jour;
    }

    public void setJour(String jour) {
        this.jour = jour;
    }

    public String getHeure() {
        return heure;
    }

    public void setHeure(String heure) {
        this.heure = heure;
    }

    public String getMatiere() {
        return matiere;
    }

    public void setMatiere(String matiere) {
        this.matiere = matiere;
    }

    public String getProfesseur() {
        return professeur;
    }

    public void setProfesseur(String professeur) {
        this.professeur = professeur;
    }

    public String getNiveau() {
        return niveau;
    }

    public void setNiveau(String niveau) {
        this.niveau = niveau;
    }

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
    }

    public String getTelephone() {
        return telephone;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }


    //Default Constructor
    public Horaire() {

    }
}
