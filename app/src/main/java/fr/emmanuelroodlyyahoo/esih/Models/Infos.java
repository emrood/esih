package fr.emmanuelroodlyyahoo.esih.Models;

import java.io.Serializable;
import java.sql.Date;

/**
 * Created by Emmanuel Roodly on 21/09/2017.
 */

public class Infos implements Serializable{

    private String thumbnail;
    private String description;
    private String titre;
    private String detail;
    private String path;
    private boolean important;
    private int views;
    private String created;
    private String img;
    private String video;

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }

    public String getVideo() {
        return video;
    }

    public void setVideo(String video) {
        this.video = video;
    }


    public String getObjectId() {
        return objectId;
    }

    public void setObjectId(String objectId) {
        this.objectId = objectId;
    }

    private String objectId;

    public String getCreated() {
        return created;
    }

    public void setCreated(String created) {
        this.created = created;
    }


    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }



    public boolean isImportant() {
        return important;
    }

    public void setImportant(boolean important) {
        this.important = important;
    }

    public int getViews() {
        return views;
    }

    public void setViews(int views) {
        this.views = views;
    }



    public String getDetail() {
        return detail;
    }

    public void setDetail(String detail) {
        this.detail = detail;
    }


    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getTitre() {
        return titre;
    }

    public void setTitre(String titre) {
        this.titre = titre;
    }



    public String getThumbnail() {
        return thumbnail;
    }

    public void setThumbnail(String thumbnail) {
        this.thumbnail = thumbnail;
    }



    public Infos(String thumbnail, String description, String titre) {
        this.thumbnail = thumbnail;
        this.description = description;
        this.titre = titre;
    }

    public Infos(){
        this.titre = "Titre";
        this.description = "Description";
        this.thumbnail = "/path";
        this.detail = "null";
    }


}
