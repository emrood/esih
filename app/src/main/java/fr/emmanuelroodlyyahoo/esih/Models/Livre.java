package fr.emmanuelroodlyyahoo.esih.Models;

import java.io.Serializable;

/**
 * Created by Emmanuel on 24-Nov-17.
 */

//auteur Titre cat1 cat2 cat3 cover created
public class Livre implements Serializable {

    private String Titre;
    private String link;
    private String cat1;
    private String cat2;
    private String cat3;
    private String cover;
    private String created;
    private String auteur;

    public String getAuteur() {
        return auteur;
    }

    public void setAuteur(String auteur) {
        this.auteur = auteur;
    }



    public String getTitre() {
        return Titre;
    }

    public void setTitre(String titre) {
        Titre = titre;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public String getCat1() {
        return cat1;
    }

    public void setCat1(String cat1) {
        this.cat1 = cat1;
    }

    public String getCat2() {
        return cat2;
    }

    public void setCat2(String cat2) {
        this.cat2 = cat2;
    }

    public String getCat3() {
        return cat3;
    }

    public void setCat3(String cat3) {
        this.cat3 = cat3;
    }

    public String getCover() {
        return cover;
    }

    public void setCover(String cover) {
        this.cover = cover;
    }

    public String getCreated() {
        return created;
    }

    public void setCreated(String created) {
        this.created = created;
    }

    public Livre() {
        this.Titre = "Titre";
        this.link = "lien";
        this.cat1 = "categorie";
        this.cover = "couverture";
        this.auteur = "auteur";
    }
}
