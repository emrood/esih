package fr.emmanuelroodlyyahoo.esih.Models;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 * Created by Emmanuel on 10/19/2017.
 */

public class Message {
    public String getSender() {
        return sender;
    }

    public void setSender(String sender) {
        this.sender = sender;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public String getCreated() {
        return created;
    }

    public void setCreated(String created) {
        this.created = created;
    }

    String sender;
    String body;
    String created;
    String objectId;

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    String avatar;

    public void setObjectId(String objectId) {
        this.objectId = objectId;
    }

    public String getOwnerId() {
        return ownerId;
    }

    public void setOwnerId(String ownerId) {
        this.ownerId = ownerId;
    }

    String ownerId;

    public String getObjectId() {
        return objectId;
    }






    public String getNiveau() {
        return niveau;
    }

    public void setNiveau(String niveau) {
        this.niveau = niveau;
    }

    String niveau;

    public Message(String sender, String body, String niveau) {
        this.sender = sender;
        this.body = body;
        this.niveau = niveau;
        DateFormat df = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");
        this.created = df.format(Calendar.getInstance().getTime());
    }

    public Message() {

    }

}
