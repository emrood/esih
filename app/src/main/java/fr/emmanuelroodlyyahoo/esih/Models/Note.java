package fr.emmanuelroodlyyahoo.esih.Models;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by Emmanuel on 9/26/2017.
 */

public class Note implements Serializable {

    //Attributs
    private String matiere;
    private String nfinal;
    private String nintra;
    private String session;
    private String niveau;
    private String devoir;
    private String created;
    private String ntotal;
    private String ID;

    public String getID() {
        return ID;
    }

    public void setID(String ID) {
        this.ID = ID;
    }



    //Getters and Setters

    public String getNtotal() {
        return ntotal;
    }

    public void setNtotal(String ntotal) {
        this.ntotal = ntotal;
    }


    public String getMatiere() {
        return matiere;
    }

    public void setMatiere(String matiere) {
        this.matiere = matiere;
    }

    public String getNfinal() {
        return nfinal;
    }

    public void setNfinal(String nfinal) {
        this.nfinal = nfinal;
    }

    public String getNintra() {
        return nintra;
    }

    public void setNintra(String nintra) {
        this.nintra = nintra;
    }

    public String getSession() {
        return session;
    }

    public void setSession(String session) {
        this.session = session;
    }

    public String getNiveau() {
        return niveau;
    }

    public void setNiveau(String niveau) {
        this.niveau = niveau;
    }

    public String getDevoir() {
        return devoir;
    }

    public void setDevoir(String devoir) {
        this.devoir = devoir;
    }

    public String getCreated() {
        return created;
    }

    public void setCreated(String created) {
        this.created = created;
    }

    public Note(String matiere, String nfinal, String nintra, String session, String niveau) {
        this.matiere = matiere;
        this.nfinal = nfinal;
        this.nintra = nintra;
        this.session = session;
        this.niveau = niveau;
    }

    public Note() {

    }

    public static Note fromHashMap(Map hashMap){

        Note note = new Note();
        try {
            note.setMatiere(hashMap.get("matiere").toString());
            note.setDevoir(hashMap.get("devoir").toString());
            note.setNfinal(hashMap.get("nfinal").toString());
            note.setNintra(hashMap.get("nintra").toString());
            note.setNtotal(hashMap.get("ntotal").toString());
            note.setNiveau(hashMap.get("niveau").toString());
            note.setSession(hashMap.get("session").toString());
        }catch (Exception e){
            e.printStackTrace();
        }
        return note;
    }



    public static ArrayList<Note> fromMap(List<Map> map){
        ArrayList<Note> notes = new ArrayList<>();
        for( int i = 0; i <= map.size(); i++){
            try{
                Note h = Note.fromHashMap(map.get(i));
                if(h != null){
                    notes.add(h);
                }
            }catch (Exception e){
                e.printStackTrace();
                continue;
            }
        }

        return notes;
    }
}
