package fr.emmanuelroodlyyahoo.esih;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.widget.Toast;

import com.backendless.Backendless;
import com.backendless.BackendlessUser;
import com.backendless.async.callback.AsyncCallback;
import com.backendless.exceptions.BackendlessFault;

import fr.emmanuelroodlyyahoo.esih.Activities.LoginActivity;
import fr.emmanuelroodlyyahoo.esih.Activities.MainActivity;
import fr.emmanuelroodlyyahoo.esih.utils.Defaults;

public class Splash extends Activity {

    int time = 4000;
    public String userToken;
    public Intent start;
    public boolean valid;
    SharedPreferences mUser;
    SharedPreferences.Editor editor;
    String mail;
    String password;
    SharedPreferences registered;
    boolean register;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        Backendless.setUrl( Defaults.SERVER_URL );
        Backendless.initApp( getApplicationContext(), Defaults.APPLICATION_ID, Defaults.API_KEY );
        mUser = getBaseContext().getSharedPreferences("User", Context.MODE_PRIVATE);
        registered = getBaseContext().getSharedPreferences("Register", Context.MODE_PRIVATE);
        register = registered.getBoolean("Test", false);
        mail = mUser.getString("Email", "");
        password = mUser.getString("Pass", "");

        if(!register){
            registerDevice();
        }


        //userToken = UserTokenStorageFactory.instance().getStorage().get();
        start = new Intent(Splash.this, LoginActivity.class);

        if(mail != "" && password != ""){
            Backendless.UserService.login(mail, password, new AsyncCallback<BackendlessUser>() {
                @Override
                public void handleResponse(BackendlessUser response) {
                    //Toast.makeText(Splash.this, "Log in succesfull", Toast.LENGTH_SHORT).show();
                    Log.d("DEBUG", response.toString());
                    start = new Intent(Splash.this, MainActivity.class);
                    start.putExtra("userInfo", response);
                }

                @Override
                public void handleFault(BackendlessFault fault) {
                    Log.d("DEBUG", fault.toString());
                    start = new Intent(Splash.this, LoginActivity.class);
                }
            });

        }


        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                startActivity(start);
                overridePendingTransition(R.anim.right, R.anim.left);
                finish();

            }
        }, time);
    }

    public  void registerDevice(){
        Backendless.Messaging.registerDevice("1078233325452", new AsyncCallback<Void>() {
            @Override
            public void handleResponse(Void response) {
                //Toast.makeText(Splash.this, "Registered", Toast.LENGTH_SHORT).show();
                editor = registered.edit();
                editor.putBoolean("Test", true);
                editor.apply();
            }

            @Override
            public void handleFault(BackendlessFault fault) {
                //Toast.makeText(Splash.this, fault.toString(), Toast.LENGTH_SHORT).show();
                editor = registered.edit();
                editor.putBoolean("Test", false);
                editor.apply();
            }
        });
    }

    @Override
    public void onBackPressed() {
        moveTaskToBack(true);
    }


    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
    }
}
