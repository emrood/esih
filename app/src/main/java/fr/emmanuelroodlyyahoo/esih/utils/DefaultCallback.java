package fr.emmanuelroodlyyahoo.esih.utils;

/**
 * Created by Emmanuel on 2/7/2018.
 */

import android.app.ProgressDialog;
import android.content.Context;
import android.widget.Toast;
import com.backendless.async.callback.BackendlessCallback;
import com.backendless.exceptions.BackendlessFault;

public class DefaultCallback<T> extends BackendlessCallback<T>
{
    Context context;
    ProgressDialog progressDialog;

    public DefaultCallback( Context context )
    {
        this.context = context;
        Toast.makeText(context, "Chargement", Toast.LENGTH_SHORT).show();
    }

    public DefaultCallback( Context context, String loadingMessage )
    {
        this.context = context;
        Toast.makeText(context, "Chargement", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void handleResponse( T response )
    {
        progressDialog.cancel();
    }

    @Override
    public void handleFault( BackendlessFault fault )
    {
        progressDialog.cancel();
        Toast.makeText( context, fault.getMessage(), Toast.LENGTH_LONG ).show();
    }
}