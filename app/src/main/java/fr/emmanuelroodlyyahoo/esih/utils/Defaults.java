package fr.emmanuelroodlyyahoo.esih.utils;

public class Defaults {

    public static final String APPLICATION_ID = "A1CBE513-5446-4312-FF79-C93DA4C45000";
    public static final String API_KEY = "770EBAF5-E9ED-E035-FF22-55EDF892F500";
    public static final String SERVER_URL = "https://api.backendless.com";

    public static final String GOOGLE_PROJECT_ID = "emrood-174505";

    public static final String DEFAULT_CHANNEL = "default";
    public static final int CAMERA_REQUEST = 101;
    public static final int URL_REQUEST = 102;
    public static final String DATA_TAG = "data";
    public static final String DEFAULT_PATH_ROOT = "devoir/received";
    public static final String DEFAULT_PATH_AVATAR = "avatar";

}
                                            