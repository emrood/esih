package fr.emmanuelroodlyyahoo.esih.utils;

import android.support.v7.util.DiffUtil;

import java.util.ArrayList;
import java.util.List;

import fr.emmanuelroodlyyahoo.esih.Models.Message;

/**
 * Created by Emmanuel on 2/2/2018.
 */

public class MessageDiffCallback extends DiffUtil.Callback {

    private List<Message> mOldList;
    private List<Message> mNewList;

    public MessageDiffCallback(List<Message> oldList, List<Message> newList) {
        this.mOldList = oldList;
        this.mNewList = newList;
    }

    @Override
    public int getOldListSize() {
        return mOldList.size();
    }

    @Override
    public int getNewListSize() {
        return mNewList.size();
    }

    @Override
    public boolean areItemsTheSame(int oldItemPosition, int newItemPosition) {
        // add a unique ID property on Contact and expose a getId() method
        return mOldList.get(oldItemPosition).getObjectId() == mNewList.get(newItemPosition).getObjectId();
    }

    @Override
    public boolean areContentsTheSame(int oldItemPosition, int newItemPosition) {
        Message oldContact = mOldList.get(oldItemPosition);
        Message newContact = mNewList.get(newItemPosition);

        if (oldContact.getObjectId() == newContact.getObjectId()) {
            return true;
        }
        return false;
    }
}
