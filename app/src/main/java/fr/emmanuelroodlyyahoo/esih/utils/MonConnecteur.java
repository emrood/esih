package fr.emmanuelroodlyyahoo.esih.utils;

/**
 * Created by Emmanuel Roodly on 29/07/2017.
 */

public interface MonConnecteur {
    public void getValueFromChild(String text);
}
