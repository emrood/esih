package fr.emmanuelroodlyyahoo.esih.utils;

/**
 * Created by Emmanuel on 2/7/2018.
 */


import com.backendless.push.BackendlessBroadcastReceiver;
import com.backendless.push.BackendlessPushService;

public class PushReceiver extends BackendlessBroadcastReceiver {
    @Override
    public Class<? extends BackendlessPushService> getServiceClass() {
        return PushService.class;
    }
}